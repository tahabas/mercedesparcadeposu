
<!DOCTYPE html>
<html lang="en" >
<!-- begin::Head -->
<head>
    <meta charset="utf-8" />
    <title>
       Yönetim Paneli
    </title>
    <meta name="description" content="Latest updates and statistic charts">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!--begin::Web font -->
    <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
    <script>
        WebFont.load({
            google: {"families":["Poppins:300,400,500,600,700","Roboto:300,400,500,600,700"]},
            active: function() {
                sessionStorage.fonts = true;
            }
        });
    </script>
    <!--end::Web font -->
    <!--begin::Base Styles -->
    <link href="/tema/assets/vendors/base/vendors.bundle.css" rel="stylesheet" type="text/css" />
    <link href="/tema/assets/demo/default/base/style.bundle.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.9/sweetalert2.min.css">
    <!--end::Base Styles -->
    <link rel="shortcut icon" href="/tema/assets/demo/default/media/img/logo/favicon.ico" />
</head>
<!-- end::Head -->
<!-- end::Body -->
<body  class="m--skin- m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default"  >
<!-- begin:: Page -->
<div class="m-grid m-grid--hor m-grid--root m-page">
    <div class="m-grid__item m-grid__item--fluid m-grid m-grid--hor m-login m-login--signin m-login--2 m-login-2--skin-2" id="m_login" style="background-image: url(/tema/assets/app/media/img//bg/bg-3.jpg);">
        <div class="m-grid__item m-grid__item--fluid    m-login__wrapper">
            <div class="m-login__container">
                <div class="m-login__logo">
                    <a href="#">
                        <img src="/images/logo.png">
                    </a>
                </div>
                <div class="m-login__signin">
                    <div class="m-login__head">
                        <h3 class="m-login__title">
                           Yönetim Paneli
                        </h3>
                    </div>
                    @if ($error = $errors->first('password') || $error = $errors->first('email'))
                        <div class="alert alert-danger">
                            E-Posta Adresi Veya Şifre Yanlış
                        </div>
                    @endif
                    <form class="m-login__form m-form" method="POST" action="{{ route('login') }}">
                        @csrf

                        <div class="form-group m-form__group">
                            <input class="form-control m-input" id="email"   type="text" placeholder="Mail" name="email" autocomplete="off">



                        </div>
                        <div class="form-group m-form__group">
                            <input id="password" class="form-control m-input m-login__form-input--last" type="password" placeholder="Şifre" name="password">

                        </div>
                        <div class="row m-login__form-sub">
                            <div class="col m--align-left m-login__form-left">

                            </div>
                            <div class="col m--align-right m-login__form-right">

                            </div>
                        </div>
                        <div class="m-login__form-action">
                            <button  type="submit" class="btn btn-focus m-btn m-btn--pill m-btn--custom m-btn--air m-login__btn m-login__btn--primary">
                                Giriş Yap
                            </button>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>
</div>
<!-- end:: Page -->
<!--begin::Base Scripts -->
<script src="/tema/assets/vendors/base/vendors.bundle.js" type="text/javascript"></script>
<script src="/tema/assets/demo/default/base/scripts.bundle.js" type="text/javascript"></script>
<!--end::Base Scripts -->
<!--begin::Page Snippets -->
<script src="/tema/assets/snippets/custom/pages/user/login.js" type="text/javascript"></script>
<script src="https://unpkg.com/sweetalert2@7.18.0/dist/sweetalert2.all.js"></script>
@if (session('status')=='1')
    <script> Swal('Başarılı', 'Unutulan Şifrenis E-Posta Adresinize Gönderildi', 'success');</script>
@elseif(session('status')=='2')
    <script> Swal('Kullanıcı Bulunamadı', 'Kullanıcı Mevcut Değil', 'error');</script>
@endif
<!--end::Page Snippets -->
</body>
<!-- end::Body -->
</html>
<!-------------------------------------------------------------------------------->


