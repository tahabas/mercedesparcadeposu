<div class="w3l_banner_nav_left">
    <nav class="navbar nav_bottom">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header nav_2">
            <button type="button" class="navbar-toggle collapsed navbar-toggle1" data-toggle="collapse" data-target="#bs-megadropdown-tabs">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-megadropdown-tabs">
            <ul class="nav navbar-nav nav_1">
                @foreach($sol_menuler as $sol_menu)

                    @if(count($sol_alt_menuler[$sol_menu->id])>0 )
                        <li class="dropdown mega-dropdown active">
                            <a href="urun_list/{{$sol_menu->id}}" class="dropdown-toggle" data-toggle="dropdown">{{$sol_menu->ad}}<span class="caret"></span></a>
                            <div class="dropdown-menu mega-dropdown-menu w3ls_vegetables_menu">
                                <div class="w3ls_vegetables">
                                    <ul>
                                        @foreach($sol_alt_menuler[$sol_menu->id] as $sol_alt_menu)
                                            <li><a href="urun_list/{{$sol_alt_menu->id}}">{{$sol_alt_menu->ad}}</a></li>
                                            
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        </li>
                    @else
                        <li><a href="urun_list/{{$sol_menu->id}}">{{$sol_menu->ad}}</a></li>
                    @endif
                @endforeach



            </ul>
        </div><!-- /.navbar-collapse -->
    </nav>
</div>