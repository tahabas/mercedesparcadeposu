@extends('layouts.site')



@section('content')

<div class="row">
    <div class="col">
        <div id="modelsIndicators" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
                <li data-target="#modelsIndicators" data-slide-to="0" class="active"></li>
                <li data-target="#modelsIndicators" data-slide-to="1"></li>
                <li data-target="#modelsIndicators" data-slide-to="2"></li>
                <li data-target="#modelsIndicators" data-slide-to="3"></li>
            </ol>
            <div class="carousel-inner" >
                @foreach($slider as $slide)
                <div   @if ($loop->first) class="carousel-item active" @else class="carousel-item" @endif>
                    <img class="d-block w-100" src="{{$slide->resim}}" >
                </div>
                @endforeach

            </div>
            <a class="carousel-control-prev" href="#modelsIndicators" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#modelsIndicators" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>

    </div>
</div>
<hr>
<div class="row">

    @include('layouts.new.productslider') 

</div>

@endsection