@extends('layouts.site')
@section('css')
@endsection
@section('content')

<h3 class="page-title">Bİze Ulaşın</h3>    

<div class="row">
    <div class="col">

        {!! Form::open(['route'=>'iletisim.mailgonder','method'=>'POST','class'=>'form-horizontal']) !!}
        <div class="form-row">
            <div class="col-md-6">

                <div class="form-group">
                    <label for="ad"><b>Ad Soyad</b></label>
                    <input type="text" class="form-control" id="ad"  name="ad" placeholder="Ad Soyad" required>
                </div>
            </div>
            <div class="col-md-6">

                <div class="form-group">
                    <label for="mail"><b>E-Posta Adresi</b></label>
                    <input type="email" class="form-control" id="mail"  name="mail" placeholder="E-Posta Adresi" required>
                </div>
            </div>
        </div>

        <div class="form-row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="telefon"><b>Telefon Numarası</b></label>
                    <input type="text" class="form-control" id="telefon" placeholder="Telefon Numarası" required>
                </div>

            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="konu"><b>Konu</b></label>
                    <input type="text" class="form-control" id="konu" placeholder="Konu" required>
                </div>

            </div>
        </div>


        <div class="form-row">
            <div class="col ">


                <div class="form-group">
                    <label for="mesaj"><b>Mesajınız</b></label>
                    <textarea class="form-control" id="mesaj" rows="3" placeholder="Mesajınız" required></textarea>
                </div>
            </div>
        </div>

        <button type="submit" class="btn btn-warning">Gönder</button>
        {!! Form::close() !!}
    </div>




</div>

<div class="row mt-3">
    <div class="col-md-6 about-company">
        <div class="card">
            <div class="card-body">
            <p class="pr-5 "> <b>Adres : </b>Bahçekapı Mah. Sanayi Blv. Yahşi İş Merkezi No:24/6 06797 Etimesgut/Ankara Türkiye</p>
                <p class="pr-5 "> <b>Telefon : </b>(0312) 278 38 60</p>
                <p class="pr-5 "> <b>E-Posta : </b>info@mercedesparcadeposu.com</p>
            </div>
        </div>
    </div>
</div>
<div class="row mt-3">
    <div class="col">
        <iframe style="width: 100%" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3059.3436510497327!2d32.70938721538132!3d39.93370217942424!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x14d348024de1dfd1%3A0xbf681654feb3d34a!2sBah%C3%A7ekap%C4%B1+Mahallesi%2C+2473.+Cadde+No%3A24%2C+06797+Etimesgut%2FAnkara!5e0!3m2!1str!2str!4v1535719214239"  height="450" frameborder="0" style="border:0" allowfullscreen></iframe>


    </div>
</div>

@endsection
@section('icerik')

@endsection
@section('js')
@endsection
