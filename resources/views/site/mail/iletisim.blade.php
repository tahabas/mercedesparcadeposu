<!DOCTYPE html>
<html>
<head>
    <style>
        table {
            font-family: arial, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

        td, th {
            border: 1px solid #dddddd;
            text-align: left;
            padding: 8px;
        }

        tr:nth-child(even) {
            background-color: #dddddd;
        }
    </style>
</head>
<body>



<table>
    <tr>
        <th></th>
        <th></th>

    </tr>
    <tr>
        <td>İsim</td>
        <td>{{$content['name']}}</td>
    </tr>
    <tr>
        <td>E-Posta</td>
        <td>{{$content['email']}}</td>
    </tr>
    <tr>
        <td>Telefon</td>
        <td>{{$content['phone']}}</td>
    </tr>
    <tr>
        <td>Konu</td>
        <td>{{$content['subject']}}</td>
    </tr>
    <tr>
        <td>Mesaj</td>
        <td>{{$content['messagetext']}}</td>
    </tr>
</table>

</body>
</html>
