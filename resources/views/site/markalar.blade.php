@extends('layouts.site')
@section('css')
@endsection
@section('content')
<div class="container">
    <div class="w3ls_w3l_banner_nav_right_grid w3ls_w3l_banner_nav_right_grid_sub">
        <h3 class="page-title">Markalar</h3>    
        <div class="row w3ls_w3l_banner_nav_right_grid1 w3ls_w3l_banner_nav_right_grid1_veg">


            @foreach($markalar as $index=>$marka)
            @if($index % 4 == 0)
            <div class="row">
                @endif 
                <div class="col-md-3 mb-3">

                    <div class="card m-auto">
                        <img class="card-img-top img-fluid" src="{{$marka->resim}}" alt="Card image cap">
                        <div class="card-body">
                            <a href="urunler/{{$marka->id}}?sorgu=marka" class="btn-itl">
                                <span>{{$marka->ad}} ({{$marka->urun->count()}}) 
                                    <i class="fas fa-arrow-circle-right text-warning fa-lg"></i>
                                </span>
                            </a>
                        </div>
                    </div>

                </div>
                @if($index % 4 == 3 || $index == count($markalar)-1)
            </div>
            @endif 

            @endforeach

        </div>


    </div>
</div>
@endsection
@section('icerik')

@endsection
@section('js')
@endsection