@extends('layouts.site')
@section('css')
@endsection
@section('content')
<div class="page">
    <h3 class="page-title">Hakkımızda</h3>

    <div class="card">
        <div class="card-body " style="font-size: 20px;">
            <p>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Firmamız kurulduğu günden bugüne kadar ağırlıklı olarak Mercedes araçların yedek parçalarının tedariği ve satışı üzerine hizmet vermektedir.
                Firmamız bir aile şirketidir. 
                İçanadolu bölgesinde aktif olan firmamız 2008 yılından itibaren ticaret hayatına www.mercedesparcadeposu.com e-ticaret sistemini oluşturarak Mercedes araçların yedek parçalarının tedarik ve satış hizmetini ülke geneline taşımıştır.
                Ticaret anlayışımızda her zaman önceliğimiz KALİTELİ ÜRÜN daha sonrasında HESAPLI ÜRÜN olmuştur.
                Hedefimiz, sattığımız ürünlerden şikayet almadan, montaj esnasında uyum sorunu olmadan belirttiğimiz süre içinde sizlere ihtiyacınız olan ürünü en hızlı şekilde ulaştırmaktır . Bu hedefimizin sürekliliğini sağlamak için belirli kalite standartlarına  sahip, kalite ve uzun ömürlülüğünü ispatlamış markaların ürünlerini Mercedes kullanıcılarına sunmaktır.
                Kullanmakta olduğumuz yedek parça katalog programları ile yüksek verimle müşterilerimize sorunsuz bir şekilde hizmet vermekteyiz.
                Ankara il merkezinde mağazalarımızda  300 çeşit stoklu yedek parça ile Mercedes kullanıcılarının ihtiyaçları doğrultusunda hizmet vermeye devam etmekteyiz.
                E-ticaret alanında da sahip olduğumuz ticaret anlayışımızla Mercedes kullanıcılarına www.mercedesparcadeposu.com mağazamız ile 7/24 hizmet vermekteyiz.</p>
        </div>
    </div>

</div>



@endsection
@section('icerik')

@endsection
@section('js')
@endsection