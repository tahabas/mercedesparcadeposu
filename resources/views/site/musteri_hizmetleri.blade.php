@extends('layouts.site')
@section('css')
@endsection
@section('content')
<div class="page">
    <h3 class="page-title">Müşteri Hizmetleri</h3>

    <div class="card">
        <div class="card-body " style="font-size: 20px;">
            <p>mercedesparcadeposu.com`da satın alma, sipariş, kargo ve diğer konularla ilgili aklınıza gelebilecek soruların cevaplarını bulabilirsiniz.</p>

            <p>Burada cevabını bulamadığınız veya hakkında daha detaylı bilgi istediğiniz her türlü sorunuz için info@mercedesparcadeposu.com adresine mail atabilir veya (0312) 278 38 60 numaralı telefonumuzdan müşteri temsilcilerini arayabilirsiniz.</p>

            <p>Siparişlerizle ilgili olarak göndereceğiniz e-postalarda asla kredi kartı numaranızı ve diğer kredi kartı bilgilerinizi yazmayınız. Kredi kartı bilgilerinin bildirilmesini gerektiren durumlarda satış sorumlusu arkadaşlarımızla iletişim kurunuz.</p>

            <p>mercedesparcadeposu.com koşulsuz müşteri memnuniyeti sağlamayı öncelikli iş prensipleri arasında tutmaktadır.</p>
        </div>
    </div>
</div>

</div>



@endsection
@section('icerik')

@endsection
@section('js')
@endsection