@extends('layouts.site')
@section('css')
@endsection
@section('content')
<div class="container">

    <h3 class="page-title">"{{$cumle}}" Arama sonuçları</h3>    

    @if( count($urunler) == 0)
    <div class="alert alert-warning" role="alert">
        "{{$cumle}}"Aramanız ile eşleşen ürün bulunamadı.
    </div>
    @endif 



    @foreach($urunler as $index=>$urun)

    @if($index % 3 == 0)
    <div class="row">
        @endif 
        <div class="col-md-4  mb-3">
            <div class="owl-item"><div class="single_product">
                    <div class="product_name">
                        <p class="manufacture_product"><a href="/urun/{{$urun->id}}">{{$urun->ad}}</a></p>
                    </div>
                    <div class="product_thumb">
                        <a class="primary_img" href="/urun/{{$urun->id}}"><img src="{{$urun->resim}}" alt=""></a>
                    </div>
                    <div class="product_code">
                                                    <p class="mb-0"><b>Parça No : </b>{{$urun->kod}}</p>
                                                    <p class="mb-0"><b>Ürün Markası : </b>{{$urun->marka->ad}}</p>
                                                    <p><b>Araç Modeli : </b>{{$urun->araba_model}}</p>
                                                </div>

                    <a class="btn-itl" href="/urun/{{$urun->id}}">
                        <span>İncele <i class="fas fa-arrow-circle-right text-warning fa-lg"></i></span>
                    </a>
                </div>
            </div>
        </div>
        @if($index % 3 == 2 || $index == count($urunler)-1)
    </div>
    @endif 

    @endforeach




    <div class="clearfix"></div>
</div>
@endsection

@section('js')
@endsection