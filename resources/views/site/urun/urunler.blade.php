@extends('layouts.site')
@section('css')
@endsection
@section('content')

<h3 class="page-title">Ürünler</h3>    

{!! $urunler->render() !!}

 
    @foreach($urunler as $index=>$urun)

    @if($index % 3 == 0)
    <div class="row mt-3">
        @endif 
        <div class="col-md-4 ">
            <div class="owl-item"><div class="single_product">
                    <div class="product_name">
                        <p class="manufacture_product"><a href="/urun/{{$urun->id}}">{{$urun->ad}}</a></p>
                    </div>
                    <div class="product_thumb">
                        <a class="primary_img" href="/urun/{{$urun->id}}"><img src="{{$urun->resim}}" alt=""></a>
                    </div>
                    <div class="product_code">
                                                    <p class="mb-0"><b>Parça No : </b>{{$urun->kod}}</p>
                                                    <p class="mb-0"><b>Ürün Markası : </b>{{$urun->marka->ad}}</p>
                                                    <p><b>Araç Modeli : </b>{{$urun->araba_model}}</p>
                                                </div>

                    <a class="btn-itl" href="/urun/{{$urun->id}}">
                        <span>İncele <i class="fas fa-arrow-circle-right text-warning fa-lg"></i></span>
                    </a>
                </div>
            </div>
        </div>
        @if($index % 3 == 2 || $index == count($urunler)-1)
    </div>
    @endif 

    @endforeach
    
@endsection

@section('js')
@endsection
