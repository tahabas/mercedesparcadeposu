@extends('layouts.site')
@section('css')
<link rel="stylesheet" type="text/css" href="{{ asset('new/plugins/jquery.fancybox/fancybox/jquery.fancybox-1.3.4.css') }}">
@endsection
@section('content')
@if($urun->kategori->resim!='')
<div class="w3l_banner_nav_right_banner3" style="background: url({{$urun->kategori->resim}}) no-repeat 0px 0px">
    <h3>{{$urun->kategori->ad}}</h3>
</div>
@endif
<div class="agileinfo_single">

    <h5>{{$urun->ad}}</h5>
    <hr>
    <div class="row">
        <div class="col-4 agileinfo_single_left">
        <a id="prod-img" href="{{$urun->resim}}"><img  src="{{$urun->resim}}" alt="{{$urun->kategori->ad}}"  class="img-fluid"></a>
        </div>

        <div class="col-8 agileinfo_single_right">

            <div class="snipcart-item block">

                <div class="snipcart-thumb agileinfo_single_right_snipcart">
                    <table  class="table condensed">
                      
                        <tr>
                            <td style="border: none"><b>Parça Numarası</b></td>
                            <td style="border: none"><span>{{$urun->kod}}</span></td>
                        </tr>
                        <tr>
                            <td><b>Stok</b></td>
                            <td><span>{{$urun->stok}}</span></td>
                        </tr>
                        <tr>
                            <td><b>Araç Modeli</b></td>
                            <td><span>{{$urun->araba_model}}</span></td>
                        </tr>
                        <tr>
                            <td><b>Model Yılı</b></td>
                            <td><span>{{$urun->araba_yil}}</span></td>
                        </tr>
                        <tr>
                            <td><b>Kasa tipid</b></td>
                            <td><span>{{$urun->araba_kasa}}</span></td>
                        </tr>
                        <tr>
                            <td><b>Ürün Markası</b></td>
                            <td><span>{{$urun->marka->ad}}</span></td>
                        </tr>
                    </table>
                </div>

            </div>
            <div class="w3agile_description">
                @if($urun->aciklama!='')
                <h4>Açıklama :</h4>
                <div>{{$urun->aciklama}}</div>
                @endif
            </div>

        </div>
    </div>
    <div class="row">

        <div class="col-md-12">

            <div style="    background-color: #fffaeb;
                 border: 1px solid;
                 margin: 20px 0;
                 padding-top: 20px;">
                <p align="center"><strong><span style="color: #333333;"><span style="font-family: Open Sans, sans-serif;"><span style="font-size: small;">PAR&Ccedil;A NO : {{$urun->kod}}</span></span></span></strong></p>
                <p align="center"><strong><span style="color: #333333;"><span style="font-family: Open Sans, sans-serif;"><span style="font-size: small;">DİGER PARCALAR CIKMA VE SIFIR OLARAK MEVCUTTUR...</span></span></span></strong></p>
                <p align="center"><strong><span style="color: #333333;"><span style="font-family: Open Sans, sans-serif;"><span style="font-size: small;">ANKARA İ&Ccedil;İ İ&Ccedil;İN AYNI G&Uuml;N ADRES TESLİMİ YAPILIR...</span></span></span></strong></p>
                <p align="center"><span style="color: #333333;"> <strong><span style="font-family: Open Sans, sans-serif;"><span style="font-size: small;">DİĞER İLLER İ&Ccedil;İN AYNI G&Uuml;N U&Ccedil;AK KARGO  VE</span></span></strong></span></p>
                <p align="center"><strong><span style="color: #333333;"><span style="font-family: Open Sans, sans-serif;"><span style="font-size: small;">ANLASMALI KARGOLARIMIZLA PAR&Ccedil;A G&Ouml;NDERİLMEKTEDİR...</span></span></span></strong></p>
                <p align="center"><strong><span style="color: #333333;"><span style="font-family: Open Sans, sans-serif;"><span style="font-size: small;">KAPIDA &Ouml;DEME SİSTEMİMİZ VARDIR.</span></span></span></strong></p>

                <p align="center"><strong><span style="color: #333333;"><span style="font-family: Open Sans, sans-serif;"><span style="font-size: small;">www.meranmercedes.com</span></span></span></strong></p> <p style="text-align: center;"><span style="font-size: x-large;"><strong>0312 278 64 40</strong></span></p>
            </div>
            <img class="img-fluid" src="/images/kargolar.png">
        </div>
    </div>
    <div class="clearfix"> </div>
</div>


@endsection
@section('icerik')

@endsection
@section('js')
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4/jquery.min.js"></script>
<script src="{{ asset('new/plugins/jquery.fancybox/fancybox/jquery.fancybox-1.3.4.pack.js')}}"></script>
<script type="text/javascript">
        var j14 = jQuery.noConflict();

        j14(document).ready(function () {
            j14("a#prod-img").fancybox();
        });
    </script>

	</script>
@endsection
