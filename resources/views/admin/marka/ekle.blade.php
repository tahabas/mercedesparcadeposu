@extends('admin.genel.template')

@section('css')
    <link rel="stylesheet" href="/tema/assets/plugins/imagecrop/dist/cropper.css">
@endsection

@section('content')
    <!-- CROP MODAL BAŞLANGIÇ-------------------------------------------------------------------------------------------------------->
    <!-- CROP MODAL BAŞLANGIÇ-------------------------------------------------------------------------------------------------------->
    <!-- CROP MODAL BAŞLANGIÇ-------------------------------------------------------------------------------------------------------->
    <!-- CROP MODAL BAŞLANGIÇ-------------------------------------------------------------------------------------------------------->

    <div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalLabel">Crop the image</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="img-container">
                        <img id="image" src="https://avatars0.githubusercontent.com/u/3456749">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="button" class="btn btn-primary" id="crop">Crop</button>
                </div>
            </div>
        </div>
    </div>

    <!--CROP MODAL BİTİŞ -------------------------------------------------------------------------------------------------------->
    <!--CROP MODAL BİTİŞ -------------------------------------------------------------------------------------------------------->
    <!--CROP MODAL BİTİŞ -------------------------------------------------------------------------------------------------------->
    <!--CROP MODAL BİTİŞ -------------------------------------------------------------------------------------------------------->
    <!--CROP MODAL BİTİŞ -------------------------------------------------------------------------------------------------------->

    <div class="m-portlet">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        Marka Ekle
                    </h3>
                </div>
            </div>
        </div>
        <div class="m-portlet__body">

            {!! Form::open(['route'=>'marka.store','method'=>'POST','class'=>'form-horizontal']) !!}
            <ul class="nav nav-pills nav-fill" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active show" data-toggle="tab" href="#m_tabs_5_1">
                        marka Bilgileri
                    </a>
                </li>
            </ul>
            <div id="app" class="tab-content">
                <image   style="width: 320px; height: 320px;" v-bind:src="resim"></image>
                <div class="tab-pane active show" id="m_tabs_5_1" role="tabpanel">

                    <div class="m-portlet__body">
                        <div class="row">

                            <input type="hidden" name="resim" v-bind:value="resim">
                            <div class="col-md-12">
                                <div class="form-group m-form__group row">
                                    <label class="col-form-label col-lg-3 col-sm-12">
                                        marka Görseli
                                    </label>
                                    <div class="col-lg-7 col-md-9 col-sm-12">
                                        <input type="file" class="btn btn-outline-info btn-lg" onchange="readURL(this);" style="width: 100%" id="inputs" name="image" accept="image/*">


                                    </div>
                                </div>

                                <div class="form-group m-form__group row">
                                    <label class="col-form-label col-lg-3 col-sm-12">
                                        marka Adı*
                                    </label>
                                    <div class="col-lg-7 col-md-9 col-sm-12">
                                        <input type="text" class="form-control m-input" required name="txtMarkaAdi"
                                               data-toggle="m-tooltip">

                                    </div>
                                </div>

                              


                            </div>


                        </div>
                    </div>
                    <div class="m-portlet__foot m-portlet__foot--fit">
                        <div class="m-form__actions m-form__actions">

                        </div>
                    </div>

                </div>

                <div class="row">
                    <div>
                        <button type="submit" class="btn btn-success">
                            Kaydet
                        </button>

                    </div>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
    </div>
@endsection
@section('js')
    <script src="/tema/assets/demo/default/custom/components/base/sweetalert2.js" type="text/javascript"></script>
    <script src="/tema/assets/plugins/imagecrop/dist/cropper.js"></script>
    @if (session('status')=='1')
        <script> Swal('Başarılı', 'İşlem Başarılı', 'success');</script>
    @elseif(session('status')=='0')
        <script> Swal('Başarısız', 'İşlem Sırasında Bir Sorun Oluştu', 'error');</script>
    @endif
    <script>

        var app = new Vue({
            el: '#app',
            data: {
                ozellikler:[],
                sonid:0,
                resim:''

            },
            methods: {


            }
        });
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    app.resim=e.target.result;

                };

                reader.readAsDataURL(input.files[0]);
            }
        }
        window.addEventListener('DOMContentLoaded', function () {

            var avatar = document.getElementById('avatar');
            var image = document.getElementById('image');
            var input = document.getElementById('input');
            var $progress = $('.progress');
            var $progressBar = $('.progress-bar');
            var $alert = $('.alert');
            var $modal = $('#modal');
            var cropper;

            $('[data-toggle="tooltip"]').tooltip();

            input.addEventListener('change', function (e) {
                var files = e.target.files;
                var done = function (url) {
                    input.value = '';
                    image.src = url;
                    $alert.hide();
                    $modal.modal('show');
                };
                var reader;
                var file;
                var url;

                if (files && files.length > 0) {
                    file = files[0];

                    if (URL) {
                        done(URL.createObjectURL(file));
                    } else if (FileReader) {
                        reader = new FileReader();
                        reader.onload = function (e) {
                            done(reader.result);
                        };
                        reader.readAsDataURL(file);
                    }
                }
            });

            $modal.on('shown.bs.modal', function () {
                cropper = new Cropper(image, {
                    aspectRatio: 1,
                    viewMode: 0,
                });
            }).on('hidden.bs.modal', function () {
                cropper.destroy();
                cropper = null;
            });

            document.getElementById('crop').addEventListener('click', function () {
                var initialAvatarURL;
                var canvas;

                $modal.modal('hide');

                if (cropper) {
                    canvas = cropper.getCroppedCanvas({
                        width: 640,
                        height: 640,
                    });


                    app.resim=canvas.toDataURL();

                    setTimeout(function () {
                    }, 300);


                }
            });
        });
    </script>
@endsection
