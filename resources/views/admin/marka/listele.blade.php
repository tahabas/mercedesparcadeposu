@extends('admin.genel.template')

@section('css')
@endsection

@section('content')
    <div class="m-portlet m-portlet--mobile">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        Markalar
                    </h3>
                </div>
            </div>
            <div class="m-portlet__head-tools">
                <ul class="m-portlet__nav">
                    <li class="m-portlet__nav-item">
                    </li>
                </ul>
            </div>
        </div>
        <div class="m-portlet__body">
            <!--begin: Search Form -->
            <div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30">
                <div class="row align-items-center">
                    <div class="col-xl-8 order-2 order-xl-1">
                        <div class="form-group m-form__group row align-items-center">
                            <div class="col-md-4">
                                <div class="form-group m-form__group">

                                    <div class="input-group">
                                        <input type="text" class="form-control" id="aranacakkelime" placeholder="Marka adı giriniz..">
                                        <div class="input-group-append">
                                            <button onclick="filtrele();" class="btn btn-secondary" type="button">
                                                Filtrele
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 order-1 order-xl-2 m--align-right">
                        <a href="{{url('/admin/marka/create')}}"
                           class="btn btn-primary m-btn m-btn--custom m-btn--icon m-btn--air m-btn--pill">
												<span>
													<i class="la la-cart-plus"></i>
													<span>
														Yeni Marka
													</span>
												</span>
                        </a>
                        <div class="m-separator m-separator--dashed d-xl-none"></div>
                    </div>
                </div>
            </div>
            <!--end: Search Form -->
            <!--begin: Datatable -->
            <table class="m-datatable" id="html_table" width="100%">
                <thead>

                <tr>

                    <th  title="Field #1">
                        Seç
                    </th>

                    <th  title="Field #3">
                        Marka Adı
                    </th>
                    <th  title="Field #10">
                        Durum
                    </th>
                    <th  title="Field #10">
                        İşlemler
                    </th>

                </tr>

                </thead>
                <tbody>
                @foreach($markalar as $marka)
                    <tr>

                        <td>
                            <label class="m-checkbox">
                                <input type="checkbox" checked="checked">
                                <span></span>
                            </label>

                        </td>

                        <td>
                            <span > {{$marka->ad}}</span>
                        </td>
                        <td>

                        </td>
                        <td>




                            {!!Form::model($marka,['route'=>['marka.destroy',$marka->id],'method'=>'DELETE','onsubmit'=>'return validateForm()']) !!}
                            <span style="overflow: visible; width: 110px;">
                                <a href="/admin/marka/{{$marka->id}}/edit"
                                   class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill"
                                   title="Edit details">
                                    <i class="la la-edit"></i>
                                </a>

                                <button type="submit"
                                        class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill"
                                        title="Delete">
                                    <i class="la la-trash"></i>
                                </button>

                            </span>
                            {!! Form::close() !!}
                        </td>


                    </tr>
                @endforeach
                </tbody>
                {{$markalar->links()}}
            </table>
            <!--end: Datatable -->
        </div>
    </div>
@endsection
@section('js')
    <script>
        $('.m-datatable').mDatatable( {
            pageSize:   100,
        } );
        function  filtrele()
        {
            var str="/admin/marka?ara="+$('#aranacakkelime').val()+"&page="+"{{isset($_REQUEST['page'])?$_REQUEST['page']:'1' }}";
            window.location.assign(str);
        }
    </script>
    <script>
        function validateForm() {
            if (confirm("Menü Siliniyor, Onaylıyormusunuz?")) {
                return true;
            }
            else {
                return false;
            }

        }
    </script>
@endsection