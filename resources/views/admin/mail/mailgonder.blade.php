@extends('admin.genel.template')

@section('css')

@endsection

@section('content')

    <div class="m-portlet">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                      Toplu Mail Gönder
                    </h3>
                </div>
            </div>
        </div>
        <div class="m-portlet__body">
            {!! Form::open(['route'=>'mail.store','method'=>'POST','class'=>'form-horizontal']) !!}


            <div id="app" class="tab-content">
                <input type="hidden" name="mail" v-bind:value="JSON.stringify(maillist)">
                <div class="tab-pane active show" id="m_tabs_5_1" role="tabpanel">

                    <div class="m-portlet__body">



                        <div class="row">
                            <div class="col-md-6">
                                <table class="table table-sm m-table m-table--head-bg-brand">
                                    <thead class="thead-inverse">
                                    <tr>
                                        <th>
                                            Mail Adresi
                                        </th>
                                        <th>
                                            Sil
                                        </th>

                                    </tr>
                                    </thead>
                                    <tbody>

                                    <tr v-for="mail in maillist">
                                        <td >
                                            @{{ mail.mail }}
                                        </td>
                                        <td>
                                            <button type="button" v-on:click="mailsil(mail.mail)"
                                                    class="btn btn-outline-info btn-sm">
                                                Sil
                                            </button>
                                        </td>

                                    </tr>

                                    </tbody>
                                </table>
                                <div class="input-group">
                                    <input type="email" v-model="mail" class="form-control" placeholder="Mail Adresi Girin">
                                    <div class="input-group-append">
                                        <button class="btn btn-primary" v-on:click="mailekle" type="button">
                                            Ekle
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">

                                <div class="form-group m-form__group row">
                                    <label class="col-form-label col-lg-3 col-sm-12">
                                        Mail Konusu
                                    </label>
                                    <div class="col-lg-7 col-md-9 col-sm-12">
                                        <input type="text" class="form-control m-input" required name="konu"
                                               data-toggle="m-tooltip">

                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label class="col-form-label col-lg-3 col-sm-12">
                                        Mail İçeriği
                                    </label>
                                    <div class="col-lg-7 col-md-9 col-sm-12">
                                    <textarea  class="form-control m-input" required name="mesaj"
                                              ></textarea>

                                    </div>
                                </div>



                            </div>



                        </div>


                    </div>
                </div>
                <div class="m-portlet__foot m-portlet__foot--fit">
                    <div class="m-form__actions m-form__actions">

                    </div>
                </div>

            </div>

            <div class="row">
                <div>
                    <button type="submit" class="btn btn-success">
                        Kaydet
                    </button>

                </div>
            </div>
            {!! Form::close() !!}
        </div>


    </div>

@endsection
@section('js')
    <script src="/tema/assets/demo/default/custom/components/base/sweetalert2.js" type="text/javascript"></script>
    <script src="/tema/assets/plugins/imagecrop/dist/cropper.js"></script>
    @if (session('status')=='1')
        <script> Swal('Başarılı', 'İşlem Başarılı', 'success');</script>
    @elseif(session('status')=='0')
        <script> Swal('Başarısız', 'İşlem Sırasında Bir Sorun Oluştu', 'error');</script>
    @endif
    <script>

        var app = new Vue({
            el: '#app',
            data: {
                mail:'',
                maillist: [
                        @foreach($maillist as $mail)
                    {
                        mail: '{{$mail->mail}}'
                    },
                    @endforeach
                ]

            },
            methods: {
                mailsil: function (val) {
                    for (i = 0; i < app.maillist.length; i++) {
                        if (val == app.maillist[i].mail) {
                            app.maillist.splice(i, 1);
                        }
                    }
                },
                mailekle:function () {
                    var varmi=false;
                    for (i = 0; i < app.maillist.length; i++) {
                        if (this.mail == app.maillist[i].mail) {
                            varmi=true;
                        }
                    }
                    if (varmi==false)
                    {
                        this.maillist.push({mail:this.mail});
                    }
                    this.mail="";

                }

            }
        });

    </script>
@endsection
