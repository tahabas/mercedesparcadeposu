@extends('admin.genel.template')
@section('css')


@endsection

@section('content')
<div class="col-md-4">
    <div class="m-portlet">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        Şire Değiştir
                    </h3>
                </div>
            </div>
        </div>

        <div class="m-portlet__body">

            {!! Form::open(['route'=>'ayarlar.sifredegistir','method'=>'POST','class'=>'form-horizontal']) !!}


            <div  class="tab-content">

                <div class="tab-pane active show" id="m_tabs_5_1" role="tabpanel">

                    <div class="m-portlet__body">

                        <div id="contentdiv"  class="row">
                            <div class="form-group m-form__group">
                                <label for="exampleInputPassword1">
                                    Şifre
                                </label>
                                <input type="password" name="sifre" class="form-control m-input m-input--square" id="exampleInputPassword1" placeholder="Şifre">
                            </div>
                            <div class="form-group m-form__group">
                                <label for="exampleInputPassword1">
                                    Şifre Tekrar
                                </label>
                                <input type="password" name="sifre2" class="form-control m-input m-input--square" id="exampleInputPassword1" placeholder="Tekrar Şifre">
                            </div>
                        </div>
                        <div class="m-portlet__foot m-portlet__foot--fit">
                            <div class="m-form__actions m-form__actions">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div>
                            <button type="submit" class="btn btn-success">
                                Kaydet
                            </button>

                        </div>
                    </div>
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
</div>

@endsection


@section('js')

    <script src="/tema/assets/demo/default/custom/components/base/sweetalert2.js" type="text/javascript"></script>

    @if (session('status')=='1')
        <script> Swal('Başarılı', 'İşlem Başarılı', 'success');</script>
    @elseif(session('status')=='0')
        <script> Swal('Başarısız', 'İşlem Başarısız', 'error');</script>

    @elseif(session('status')=='2')
        <script> Swal('Başarısız', 'Girdiğiniz Şifreler Aynı Değil', 'error');</script>
    @endif



@endsection