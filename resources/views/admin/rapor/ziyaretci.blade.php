@extends('admin.genel.template')

@section('css')
@endsection

@section('content')
    <div class="m-portlet m-portlet--mobile">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                      Ziyaretçi Raporları
                    </h3>
                </div>
            </div>
            <div class="m-portlet__head-tools">
                <ul class="m-portlet__nav">
                    <li class="m-portlet__nav-item">
                    </li>
                </ul>
            </div>
        </div>
        <div class="m-portlet__body">
            <!--begin: Search Form -->
            <div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30">
                <div class="row align-items-center">
                    <div class="col-xl-12 order-2 order-xl-1">
                        <div class="form-group m-form__group row align-items-center">
                            <div class="col-md-12">
<h4 class="m--align-center">Toplam Ziyaret: {{count($data)}}</h4>

                            </div>
                            <div class="col-md-4">
                                <h4>Bölgeler</h4>
                                <div class="m-list-timeline">
                                    <div class="m-list-timeline__items">
                                        @foreach($bolgeler as $bolge=>$adet)
                                            <div class="m-list-timeline__item">
                                                <span class="m-list-timeline__badge"></span>
                                                <span class="m-list-timeline__text">
																		{{$bolge}}
																	</span>
                                                <span class="m-list-timeline__time">
                                                <b>
																		{{$adet}} Ziyaret</b>
																	</span>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <h4>Ip Adresleri</h4>
                                <div class="m-list-timeline">
                                    <div class="m-list-timeline__items">
                                        @foreach($ipler as $ip=>$adet)
                                            <div class="m-list-timeline__item">
                                                <span class="m-list-timeline__badge"></span>
                                                <span class="m-list-timeline__text">
																		{{$ip}}
																	</span>
                                                <span class="m-list-timeline__time">
                                                <b>
																		{{$adet}} Ziyaret
                                                    </b>
																	</span>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                {!! Form::open(['route'=>'admin.rapor.ziyaretci','method'=>'POST','id'=>'raporform','class'=>'form-horizontal']) !!}
                                <div class="form-group m-form__group row">
                                    <label class="col-form-label col-lg-3 col-sm-12">
                                        Zaman Aralığı
                                    </label>
                                    <div>
                                        <input style="display: none;" type="hidden" id="tarih2" value="tarih2" name="tarih2">
                                        <input style="display:none;" type="hidden" id="tarih1" value="tarih1" name="tarih1">
                                        <div class='input-group pull-right' id='m_daterangepicker_6'>
                                            <input value="{{$tariharaligi}}" type='text' class="form-control m-input" readonly
                                                   placeholder="Tarih Aralığı Seçiniz."/>
                                            <div class="input-group-append">
													<span class="input-group-text">
														<i class="la la-calendar-check-o"></i>
													</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <!--end: Search Form -->
            <!--begin: Datatable -->
            <table class="m-datatable" width="100%">
                <thead>

                <tr>


                    <th title="Field #3">
                        Ülke
                    </th>
                    <th title="Field #10">
                       Ip Adresi
                    </th>
                    <th title="Field #10">
                       Şehir
                    </th>
                    <th title="Field #10">
                        Ziyaret Zamanı
                    </th>
                </tr>

                </thead>
                <tbody>

                <tr>

@foreach($data as $d)

                    <td>
                        <span>{{ $d->ulke }}</span>
                    </td>
                    <td>
                        <span>{{ $d->ip }}</span>
                    </td>
                    <td>
                        <span>{{ $d->sehir }}</span>

                    </td>

                    <td>
                        <span>{{ $d->created_at }}</span>

                    </td>
                </tr>
@endforeach
                </tbody>

            </table>
            <!--end: Datatable -->
        </div>
    </div>
@endsection
@section('js')
    <script>
        moment.locale('tr');
        var a = moment().subtract(29, "days"),
            t = moment();
        $("#m_daterangepicker_6").daterangepicker({
            buttonClasses: "m-btn btn",
            applyClass: "btn-primary",
            cancelClass: "btn-secondary",
 applyLabel:'Onayla',
            cancelLabel:'Vazgeç',
            startDate: a,
            endDate: t,
            ranges: {
                "Bugün": [moment(), moment()],
                "Dün": [moment().subtract(1, "days"), moment().subtract(1, "days")],
                "Son 7 Gün": [moment().subtract(6, "days"), moment()],
                "Son 30 Gün": [moment().subtract(29, "days"), moment()],
                "Bu Ay": [moment().startOf("month"), moment().endOf("month")],
                "Geçen Ay": [moment().subtract(1, "month").startOf("month"), moment().subtract(1, "month").endOf("month")]
            }
        }, function (a, t, n) {
            $('#tarih1').val(a.format("YYYY-MM-DD"));
            $('#tarih2').val(t.format("YYYY-MM-DD"));
            $("#m_daterangepicker_6 .form-control").val(a.format("MM/DD/YYYY") + " - " + t.format("MM/DD/YYYY"));
            $('#raporform').submit();
        });
        $('.m-datatable').mDatatable({
            pageSize: 100,
        });

        function filtrele() {

            window.location.assign(str);
        }
    </script>

@endsection