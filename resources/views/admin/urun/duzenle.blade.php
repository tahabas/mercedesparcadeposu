@extends('admin.genel.template')

@section('css')
    <link rel="stylesheet" href="/tema/assets/plugins/imagecrop/dist/cropper.css">
    <link href="/tema/assets/plugins/gallery/gallery.css" rel="stylesheet">
@endsection

@section('content')
    <!-- CROP MODAL BAŞLANGIÇ-------------------------------------------------------------------------------------------------------->
    <!-- CROP MODAL BAŞLANGIÇ-------------------------------------------------------------------------------------------------------->
    <!-- CROP MODAL BAŞLANGIÇ-------------------------------------------------------------------------------------------------------->
    <!-- CROP MODAL BAŞLANGIÇ-------------------------------------------------------------------------------------------------------->

    <div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalLabel">Crop the image</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="img-container">
                        <img id="image" src="https://avatars0.githubusercontent.com/u/3456749">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="button" class="btn btn-primary" id="crop">Crop</button>
                </div>
            </div>
        </div>
    </div>

    <!--CROP MODAL BİTİŞ -------------------------------------------------------------------------------------------------------->
    <!--CROP MODAL BİTİŞ -------------------------------------------------------------------------------------------------------->
    <!--CROP MODAL BİTİŞ -------------------------------------------------------------------------------------------------------->
    <!--CROP MODAL BİTİŞ -------------------------------------------------------------------------------------------------------->
    <!--CROP MODAL BİTİŞ -------------------------------------------------------------------------------------------------------->

    <div class="m-portlet">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        Ürün Düzenle
                    </h3>
                </div>
            </div>
        </div>
        <div class="m-portlet__body">
            {!! Form::model($urun, ['route' => ['urun.update', $urun->id], 'method' => 'PUT','class'=>'form-horizontal']) !!}

            <ul class="nav nav-pills nav-fill" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active show" data-toggle="tab" href="#m_tabs_5_1">
                        Ürün Bilgileri
                    </a>
                </li>
            </ul>
            <div id="app" class="tab-content">
                <image   style="width: 320px; height: 240px;" v-bind:src="resim"></image>
                <div class="tab-pane active show" id="m_tabs_5_1" role="tabpanel">

                    <div class="m-portlet__body">
                        <div class="row">

                            <input type="hidden" name="resim" v-bind:value="resim">
                            <div class="col-md-12">
                                <div class="form-group m-form__group row">
                                    <label class="col-form-label col-lg-3 col-sm-12">
                                        Ürün Görseli
                                    </label>
                                    <div class="col-lg-7 col-md-9 col-sm-12">
                                        <input type="file" class="btn btn-outline-info btn-lg" onchange="readURL(this);" style="width: 100%" id="input" name="image" accept="image/*">


                                    </div>
                                </div>

                                <div class="form-group m-form__group row">
                                    <label class="col-form-label col-lg-3 col-sm-12">
                                        Ürün Adı*
                                    </label>
                                    <div class="col-lg-7 col-md-9 col-sm-12">
                                        <input type="text" class="form-control m-input" value="{{$urun->ad}}" required name="ad"
                                               data-toggle="m-tooltip">

                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label class="col-form-label col-lg-3 col-sm-12">
                                        Ürün Kodu*
                                    </label>
                                    <div class="col-lg-7 col-md-9 col-sm-12">
                                        <input type="text" class="form-control m-input" value="{{$urun->kod}}" required name="kod"
                                               data-toggle="m-tooltip">

                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label class="col-form-label col-lg-3 col-sm-12">
                                        Açıklama
                                    </label>
                                    <div class="col-lg-7 col-md-9 col-sm-12">
                                        <textarea required class="form-control m-input"  name="aciklama" >{{$urun->aciklama}}</textarea>

                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label class="col-form-label col-lg-3 col-sm-12">
                                        Model
                                    </label>
                                    <div class="col-lg-7 col-md-9 col-sm-12">
                                        <input type="text" class="form-control m-input" value="{{$urun->araba_model}}" required name="araba_model"
                                               data-toggle="m-tooltip">

                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label class="col-form-label col-lg-3 col-sm-12">
                                        Model Yılı
                                    </label>
                                    <div class="col-lg-7 col-md-9 col-sm-12">
                                        <input type="text" class="form-control m-input" value="{{$urun->araba_model}}" required name="araba_yil"
                                               data-toggle="m-tooltip">

                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label class="col-form-label col-lg-3 col-sm-12">
                                        Stok
                                    </label>
                                    <div class="col-lg-7 col-md-9 col-sm-12">
                                        <input type="text" class="form-control m-input" value="{{$urun->stok}}" required name="stok"
                                               data-toggle="m-tooltip">

                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label class="col-form-label col-lg-3 col-sm-12">
                                        Kasa tipi
                                    </label>
                                    <div class="col-lg-7 col-md-9 col-sm-12">
                                        <input type="text" class="form-control m-input" value="{{$urun->araba_kasa}}" required name="araba_kasa"
                                               data-toggle="m-tooltip">

                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <input required type="hidden" name="kategori_id" v-bind:value="anakat">
                                    <label class="col-form-label col-lg-3 col-sm-12">
                                        Kategori
                                    </label>
                                    <div class="col-lg-7 col-md-9 col-sm-12">
                                        <treeselect v-model="anakat" :multiple="false"
                                                    :options="kategoriler"
                                                    :open-direction="top"
                                                    placeholder="Seçiniz"/>

                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label class="col-form-label col-lg-3 col-sm-12">
                                        Marka
                                    </label>
                                    <div class="col-lg-7 col-md-9 col-sm-12">
                                        <select required class="form-control m-input" name="marka_id">
                                            <option value="">
                                                Marka Seçiniz
                                            </option>
                                            @foreach($markalar as $marka)
                                                <option {{$marka->id==$urun->marka_id?'selected="true"':''}} value="{{$marka->id}}">{{$marka->ad}}</option>
                                            @endforeach
                                        </select>

                                    </div>
                                </div>

                            </div>


                        </div>
                    </div>
                    <div class="m-portlet__foot m-portlet__foot--fit">
                        <div class="m-form__actions m-form__actions">

                        </div>
                    </div>

                </div>

                <div class="row">
                    <div>
                        <button type="submit" class="btn btn-success">
                            Kaydet
                        </button>

                    </div>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
@endsection
@section('js')
    <script src="/tema/assets/demo/default/custom/components/forms/validation/form-controls.js"
            type="text/javascript"></script>
    <script src="/tema/assets/demo/default/custom/components/forms/widgets/bootstrap-markdown.js"
            type="text/javascript"></script>
    <script src="/tema/assets/demo/default/custom/components/base/sweetalert2.js" type="text/javascript"></script>
    <script src="/tema/assets/plugins/imagecrop/dist/cropper.js"></script>
    <script src="/tema/assets/plugins/treeselectvue/dist/vue-treeselect.min.js"></script>
    <link rel="stylesheet" href="/tema/assets/plugins/treeselectvue/dist/vue-treeselect.min.css">
    <script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>
    <script src="/vendor/unisharp/laravel-ckeditor/adapters/jquery.js"></script>
    @if (session('status')=='1')
        <script> Swal('Başarılı', 'İşlem Başarılı', 'success');</script>
    @elseif(session('status')=='0')
        <script> Swal('Başarısız', 'İşlem Sphp arırasında Bir Sorun Oluştu', 'error');</script>
    @endif
    <script>
        var options = {
            filebrowserImageBrowseUrl: '/laravel-filemanager?type=Images',
            filebrowserImageUploadUrl: '/laravel-filemanager/upload?type=Images&_token=',
            filebrowserBrowseUrl: '/laravel-filemanager?type=Files',
            filebrowserUploadUrl: '/laravel-filemanager/upload?type=Files&_token='
        };
        setTimeout(function () {
            CKEDITOR.replace('aciklama', options);
        },500);

        Vue.component('treeselect', VueTreeselect.Treeselect);

        var app = new Vue({
            el: '#app',
            data: {
                ozellikler:[],
                sonid:0,
                resim:'{{$urun->resim}}',
                anakat: {{$urun->kategori_id}},
                kategoriler: [
                        @foreach($kategoriler as $ktg)
                    {
                        label: '<?php echo str_replace(PHP_EOL,'',addslashes($ktg->ad)) ?>',
                        id: {{$ktg->id}},
                        @if(count($ktg->altkategoriler)>0)
                        children: [
                                @foreach($ktg->altkategoriler as $ktg2)
                            {
                                label: '<?php echo str_replace(PHP_EOL,'',addslashes($ktg2->ad))  ?>',
                                id: {{$ktg2->id}},
                                @if(count($ktg2->altkategoriler)>0)
                                children: [
                                        @foreach($ktg2->altkategoriler as $ktg3)
                                    {
                                        label: '<?php echo str_replace(PHP_EOL,'',addslashes($ktg3->ad)) ?>',
                                        id: {{$ktg3->id}},
                                        children: [@foreach($ktg2->altkategoriler as $ktg3)
                                        {
                                            label: '<?php echo str_replace(PHP_EOL,'',addslashes($ktg3->ad)) ?>',
                                            id: {{$ktg3->id}},

                                        },
                                            @endforeach]

                                    },
                                    @endforeach]
                                @endif
                            },
                            @endforeach]
                        @endif
                    },


                    @endforeach

                ],

            },
            methods: {


            }
        });
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    app.resim=e.target.result;

                };

                reader.readAsDataURL(input.files[0]);
            }
        }
        window.addEventListener('DOMContentLoaded', function () {

            var avatar = document.getElementById('avatar');
            var image = document.getElementById('image');
            var input = document.getElementById('input');
            var $progress = $('.progress');
            var $progressBar = $('.progress-bar');
            var $alert = $('.alert');
            var $modal = $('#modal');
            var cropper;

            $('[data-toggle="tooltip"]').tooltip();

            input.addEventListener('change', function (e) {
                var files = e.target.files;
                var done = function (url) {
                    input.value = '';
                    image.src = url;
                    $alert.hide();
                    $modal.modal('show');
                };
                var reader;
                var file;
                var url;

                if (files && files.length > 0) {
                    file = files[0];

                    if (URL) {
                        done(URL.createObjectURL(file));
                    } else if (FileReader) {
                        reader = new FileReader();
                        reader.onload = function (e) {
                            done(reader.result);
                        };
                        reader.readAsDataURL(file);
                    }
                }
            });

            $modal.on('shown.bs.modal', function () {
                cropper = new Cropper(image, {
                    aspectRatio: 2,
                    viewMode: 0,
                });
            }).on('hidden.bs.modal', function () {
                cropper.destroy();
                cropper = null;
            });

            document.getElementById('crop').addEventListener('click', function () {
                var initialAvatarURL;
                var canvas;

                $modal.modal('hide');

                if (cropper) {
                    canvas = cropper.getCroppedCanvas({
                        width: 1280,
                        height: 720,
                    });


                    app.resim=canvas.toDataURL();

                    setTimeout(function () {
                    }, 300);


                }
            });
        });
    </script>
@endsection
