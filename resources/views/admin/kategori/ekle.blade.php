@extends('admin.genel.template')

@section('css')
    <link rel="stylesheet" href="/tema/assets/plugins/imagecrop/dist/cropper.css">
@endsection

@section('content')
    <!-- CROP MODAL BAŞLANGIÇ-------------------------------------------------------------------------------------------------------->
    <!-- CROP MODAL BAŞLANGIÇ-------------------------------------------------------------------------------------------------------->
    <!-- CROP MODAL BAŞLANGIÇ-------------------------------------------------------------------------------------------------------->
    <!-- CROP MODAL BAŞLANGIÇ-------------------------------------------------------------------------------------------------------->

    <div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalLabel">Crop the image</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="img-container">
                        <img id="image" src="https://avatars0.githubusercontent.com/u/3456749">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="button" class="btn btn-primary" id="crop">Crop</button>
                </div>
            </div>
        </div>
    </div>

    <!--CROP MODAL BİTİŞ -------------------------------------------------------------------------------------------------------->
    <!--CROP MODAL BİTİŞ -------------------------------------------------------------------------------------------------------->
    <!--CROP MODAL BİTİŞ -------------------------------------------------------------------------------------------------------->
    <!--CROP MODAL BİTİŞ -------------------------------------------------------------------------------------------------------->
    <!--CROP MODAL BİTİŞ -------------------------------------------------------------------------------------------------------->

    <div class="m-portlet">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        Kategori Ekle
                    </h3>
                </div>
            </div>
        </div>
        <div class="m-portlet__body">

            {!! Form::open(['route'=>'kategori.store','method'=>'POST','class'=>'form-horizontal']) !!}
            <ul class="nav nav-pills nav-fill" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active show" data-toggle="tab" href="#m_tabs_5_1">
                        Kategori Bilgileri
                    </a>
                </li>
            </ul>
            <div id="app" class="tab-content">
                <image   style="width: 1280px; height: 300px;" v-bind:src="kategoriresmi"></image>
                <div class="tab-pane active show" id="m_tabs_5_1" role="tabpanel">

                    <div class="m-portlet__body">
                        <div class="row">

                            <input type="hidden" name="kategoriresmi" v-bind:value="kategoriresmi">
                            <div class="col-md-12">
                                <div class="form-group m-form__group row">
                                    <label class="col-form-label col-lg-3 col-sm-12">
                                        Kategori Görseli
                                    </label>
                                    <div class="col-lg-7 col-md-9 col-sm-12">
                                        <input type="file" class="btn btn-outline-info btn-lg" onchange="readURL(this);" style="width: 100%" id="inputs" name="image" accept="image/*">


                                    </div>
                                </div>

                                <div class="form-group m-form__group row">
                                    <label class="col-form-label col-lg-3 col-sm-12">
                                        Kategori Adı*
                                    </label>
                                    <div class="col-lg-7 col-md-9 col-sm-12">
                                        <input type="text" class="form-control m-input" required name="txtKategoriAdi"
                                               data-toggle="m-tooltip">

                                    </div>
                                </div>

                                <div class="form-group m-form__group row">
                                    <label class="col-form-label col-lg-3 col-sm-12">
                                        Üst Kategori*
                                    </label>
                                    <div class="col-lg-7 col-md-9 col-sm-12">
                                        <select name="txtUstKategori" class="form-control m-input">
                                            <option value="0">
                                                Üst Seçiniz
                                            </option>
                                            <option value="0">
                                                Üst Kategori Yok
                                            </option>
                                            @foreach($kategoriler as $kategori)
                                                <option value="{{$kategori->id}}">{{$kategori->ad}}</option>
                                            @endforeach
                                        </select>

                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label class="col-form-label col-lg-3 col-sm-12">
                                       Sıra
                                    </label>
                                    <div class="col-lg-7 col-md-9 col-sm-12">
                                        <select name="sira" class="form-control m-input">
                                            <option selected value="99">

                                            </option>
                                            @for ($i = 1; $i < 99; $i++)
                                                <option value="{{$i}}">{{$i}}</option>
                                            @endfor

                                        </select>

                                    </div>
                                </div>

                            </div>


                        </div>
                    </div>
                    <div class="m-portlet__foot m-portlet__foot--fit">
                        <div class="m-form__actions m-form__actions">

                        </div>
                    </div>

                </div>

                <div class="row">
                    <div>
                        <button type="submit" class="btn btn-success">
                            Kaydet
                        </button>

                    </div>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
    </div>
@endsection
@section('js')
    <script src="/tema/assets/demo/default/custom/components/base/sweetalert2.js" type="text/javascript"></script>
    <script src="/tema/assets/plugins/imagecrop/dist/cropper.js"></script>
    @if (session('status')=='1')
        <script> Swal('Başarılı', 'İşlem Başarılı', 'success');</script>
    @elseif(session('status')=='0')
        <script> Swal('Başarısız', 'İşlem Sırasında Bir Sorun Oluştu', 'error');</script>
    @endif
    <script>

        var app = new Vue({
            el: '#app',
            data: {
                ozellikler:[],
                sonid:0,
                kategoriresmi:''

            },
            methods: {


            }
        });
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    app.kategoriresmi=e.target.result;

                };

                reader.readAsDataURL(input.files[0]);
            }
        }
        window.addEventListener('DOMContentLoaded', function () {

            var avatar = document.getElementById('avatar');
            var image = document.getElementById('image');
            var input = document.getElementById('input');
            var $progress = $('.progress');
            var $progressBar = $('.progress-bar');
            var $alert = $('.alert');
            var $modal = $('#modal');
            var cropper;

            $('[data-toggle="tooltip"]').tooltip();

            input.addEventListener('change', function (e) {
                var files = e.target.files;
                var done = function (url) {
                    input.value = '';
                    image.src = url;
                    $alert.hide();
                    $modal.modal('show');
                };
                var reader;
                var file;
                var url;

                if (files && files.length > 0) {
                    file = files[0];

                    if (URL) {
                        done(URL.createObjectURL(file));
                    } else if (FileReader) {
                        reader = new FileReader();
                        reader.onload = function (e) {
                            done(reader.result);
                        };
                        reader.readAsDataURL(file);
                    }
                }
            });

            $modal.on('shown.bs.modal', function () {
                cropper = new Cropper(image, {
                    aspectRatio: 3,
                    viewMode: 0,
                });
            }).on('hidden.bs.modal', function () {
                cropper.destroy();
                cropper = null;
            });

            document.getElementById('crop').addEventListener('click', function () {
                var initialAvatarURL;
                var canvas;

                $modal.modal('hide');

                if (cropper) {
                    canvas = cropper.getCroppedCanvas({
                        width: 1280,
                        height: 300,
                    });


                    app.kategoriresmi=canvas.toDataURL();

                    setTimeout(function () {
                    }, 300);


                }
            });
        });
    </script>
@endsection
