@extends('admin.genel.template')

@section('css')
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="m-portlet">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
												<span class="m-portlet__head-icon">
													<i class="flaticon-calendar"></i>
												</span>
                            <h3 class="m-portlet__head-text m--font-primary">
                                Kategori Listesi
                            </h3>
                        </div>
                    </div>
                    <div class="m-portlet__head-tools">
                        <ul class="m-portlet__nav">

                            <li class="m-portlet__nav-item m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push"
                                m-dropdown-toggle="hover" aria-expanded="true">
                                <a href="/admin/kategori/create"
                                   class="btn btn-primary m-btn m-btn--custom m-btn--icon m-btn--air m-btn--pill">
												<span>
													<i class="la la-cart-plus"></i>
													<span>
														Yeni Kategori
													</span>
												</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="m-portlet__body">
                    <div class="m-accordion m-accordion--default m-accordion--toggle-arrow" id="m_accordion_5"
                         role="tablist">
                        <!--begin::Item-->
                        @foreach($kategoriler as $kategori)
                            <div class="m-accordion__item m-accordion__item--<?php echo $kategori->altkategoriler ? 'brand' : 'success' ?>">
                                <div class="m-accordion__item-head collapsed" srole="tab"
                                     id="m_accordion_5_item_{{$kategori->id}}_head"
                                     data-toggle="collapse" href="#m_accordion_5_item_{{$kategori->id}}_body"
                                     aria-expanded="false">
													<span class="m-accordion__item-icon">



													</span>
                                    <span class="m-accordion__item-title">
                                          <div class="btn-group" role="group" aria-label="First group">
																 {!!Form::model($kategori,['route'=>['kategori.destroy',$kategori->id],'method'=>'DELETE','onsubmit'=>'return validateForm()']) !!}
                                              <button  type="submit"
                                                       class="m-btn btn btn-danger">
																	<i class="la 	la-remove"></i>
																</button>
																<a href="/admin/kategori/{{$kategori->id}}/edit"
                                                                   type="button" class="m-btn btn btn-warning">
																	<i class="la la-edit"></i>
																</a>
                                              {!! Form::close() !!}

															</div>
													<b>{{$kategori->ad}}</b>

													</span>
                                    <?php echo $kategori->altkategoriler ? '<span class="m-accordion__item-mode"></span>' : '' ?>


                                </div>
                                <div class="m-accordion__item-body collapse"
                                     id="m_accordion_5_item_{{$kategori->id}}_body"
                                     role="tabpanel" aria-labelledby="m_accordion_5_item_{{$kategori->id}}_head"
                                     data-parent="#m_accordion_5s" style="">
                                    <div class="m-accordion__item-content">
                                        @foreach($kategori->altkategoriler as $alt1)
                                            <div class="m-accordion__item m-accordion__item--<?php echo $alt1->altkategoriler ? 'brand' : 'success' ?>">
                                                <div class="m-accordion__item-head collapsed" srole="tab"
                                                     id="m_accordion_5_item_{{$alt1->id}}_head"
                                                     data-toggle="collapse"
                                                     href="#m_accordion_5_item_{{$alt1->id}}_body"
                                                     aria-expanded="false">
													<span class="m-accordion__item-icon">
														<i class="fa flaticon-user-ok"></i>
													</span>
                                                    <span class="m-accordion__item-title">
                                                          <div class="btn-group" role="group" aria-label="First group">
																 {!!Form::model($alt1,['route'=>['kategori.destroy',$alt1->id],'method'=>'DELETE','onsubmit'=>'return validateForm()']) !!}
                                                              <button  type="submit"
                                                                       class="m-btn btn btn-danger">
																	<i class="la 	la-remove"></i>
																</button>
																<a href="/admin/kategori/{{$alt1->id}}/edit"
                                                                   type="button" class="m-btn btn btn-warning">
																	<i class="la la-edit"></i>
																</a>
                                                              {!! Form::close() !!}

															</div>
													<b>{{$alt1->ad}}</b>
													</span>
                                                    <?php echo $alt1->altkategoriler ? '<span class="m-accordion__item-mode"></span>' : '' ?>
                                                </div>
                                                <div class="m-accordion__item-body collapse"
                                                     id="m_accordion_5_item_{{$alt1->id}}_body"
                                                     role="tabpanel"
                                                     aria-labelledby="m_accordion_5_item_{{$alt1->id}}_head"
                                                     data-parent="#m_accordion_5" style="">
                                                    <div class="m-accordion__item-content">
                                                        @foreach($alt1->altkategoriler as $alt2)
                                                            <div class="m-accordion__item m-accordion__item--<?php echo $alt2->altkategoriler ? 'brand' : 'success' ?>">
                                                                <div class="m-accordion__item-head collapsed"
                                                                     srole="tab"
                                                                     id="m_accordion_5_item_1_head"
                                                                     data-toggle="collapse"
                                                                     href="#m_accordion_5_item_{{$alt2->id}}_body"
                                                                     aria-expanded="false">
													<span class="m-accordion__item-icon">
														<i class="fa flaticon-user-ok"></i>
													</span>
                                                                    <span class="m-accordion__item-title">
                                                                          <div class="btn-group" role="group"
                                                                               aria-label="First group">
                                                                               {!!Form::model($alt2,['route'=>['kategori.destroy',$alt2->id],'method'=>'DELETE','onsubmit'=>'return validateForm()']) !!}
                                                                              <button  type="submit"
                                                                                       class="m-btn btn btn-danger">
																	<i class="la 	la-remove"></i>
																</button>
																<a href="/admin/kategori/{{$alt2->id}}/edit"
                                                                   type="button" class="m-btn btn btn-warning">
																	<i class="la la-edit"></i>
																</a>
                                                                              {!! Form::close() !!}


															</div>
														<b>{{$alt2->ad}}</b>
													</span>
                                                                    <?php echo $alt2->altkategoriler ? '<span class="m-accordion__item-mode"></span>' : '' ?>
                                                                </div>
                                                                <div class="m-accordion__item-body collapse"
                                                                     id="m_accordion_5_item_{{$alt2->id}}_body"
                                                                     role="tabpanel"
                                                                     aria-labelledby="m_accordion_5_item_1_head"
                                                                     data-parent="#m_accordion_5" style="">
                                                                    <div class="m-accordion__item-content">

                                                                    </div>
                                                                </div>
                                                            </div>
                                                        @endforeach
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        @endforeach

                    </div>

                </div>
            </div>


        </div>
    </div>
@endsection


@section('js')

    <script>
        function validateForm() {
            if (confirm("Menü Siliniyor, Onaylıyormusunuz?")) {
                return true;
            }
            else {
                return false;
            }

        }
    </script>



@endsection





