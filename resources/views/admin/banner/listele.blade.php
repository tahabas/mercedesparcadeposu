@extends('admin.genel.template')
@section('css')
@endsection
@section('content')
<div class="m-portlet m-portlet--mobile">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                Banner Listesi
                </h3>
            </div>
        </div>
        <div class="m-portlet__head-tools">
            <ul class="m-portlet__nav">
                <li class="m-portlet__nav-item">
                </li>
            </ul>
        </div>
    </div>
    <div class="m-portlet__body">
        <!--begin: Search Form -->
        <div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30">
            <div class="row align-items-center">
                <div class="col-xl-8 order-2 order-xl-1">
                    <div class="form-group m-form__group row align-items-center">
                        <div class="col-md-4">
                            <div class="m-input-icon m-input-icon--left">
                                <select class="form-control m-input" id="konumselect">
                                    @foreach($konumlar as $konum)
                                    <option {{$konum->id==$sonkonum?'selected="true"':''}} value="/admin/banner?konum={{$konum->id}}">
                                        {{$konum->ad}}
                                    </option>
                                   @endforeach
                                </select>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 order-1 order-xl-2 m--align-right">
                    <a href="{{url('/admin/banner/create')}}" class="btn btn-primary m-btn m-btn--custom m-btn--icon m-btn--air m-btn--pill">
                        <span>
                            <i class="la la-cart-plus"></i>
                            <span>
                                Yeni Banner
                            </span>
                        </span>
                    </a>
                    <div class="m-separator m-separator--dashed d-xl-none"></div>
                </div>
            </div>
        </div>
        <!--end: Search Form -->
        <!--begin: Datatable -->
        <div class="m-widget5">
        @foreach($bannerlist as $banner)
            <div class="m-widget5__item">
                <div class="m-widget5__pic">

                    <img class="m-widget7__img" src="{{ $banner->resim}}" alt="">
                </div>
                <div class="m-widget5__content">
                    <h4 class="m-widget5__title">
                   {{$banner->ad}}
                    </h4>
                    <span class="m-widget5__desc">
                         {{$banner->acikalama}}
                    </span>
                    <div class="m-widget5__info">
                        <span class="m-widget5__author">
                            Url:
                        </span>
                        <span class="m-widget5__info-author m--font-info">
                          {{$banner->konum}}
                        </span>



                    </div>
                </div>
                <div class="m-widget5__stats1">

                </div>
                <div class="m-widget5__stats2">

                    {!!Form::model($banner,['route'=>['banner.destroy',$banner->id],'method'=>'DELETE','onsubmit'=>'return validateForm('.$banner->id.')','id'=>'silform'.$banner->id]) !!}
                    <a href="/admin/banner/{{$banner->id}}/edit" class="btn btn-warning btn-sm">
                     Düzenle
                    </a>
                    <button type="submit" class="btn btn-danger btn-sm">
                        Sil
                    </button>
                    {!! Form::close() !!}

                </div>
            </div>
            @endforeach
            {{$bannerlist->links()}}
        </div>
    </div>

    <!--end: Datatable -->

</div>
@endsection
@section('js')
    <script src="/tema/assets/demo/default/custom/components/base/sweetalert2.js" type="text/javascript"></script>
    <script>

        function validateForm(formid) {
            var silinsinmi =false;
            swal({
                title: 'Silmek isdediğinize emin misiniz ?',
                text: "Bu işlem geri alınamaz.!",
                type: 'question',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Evet'
            }).then((result) => {
                if (result.value) {

                document.getElementById("silform"+formid).submit();
            }
        });

           return false;
        }
        $( "#konumselect" ).change(function() {

            window.location.href = $( this ).val();
        });
    </script>
@endsection