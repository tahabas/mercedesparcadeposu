@extends('admin.genel.template')

@section('css')
@endsection

@section('content')
    <div class="m-portlet">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                      Yeni Banner Ekle
                    </h3>
                </div>
            </div>
        </div>
        <div class="m-portlet__body">
            {!! Form::open(['route'=>'banner.store','method'=>'POST','class'=>'form-horizontal','onsubmit'=>'return validateForm()']) !!}
            <ul class="nav nav-pills nav-fill" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active show" data-toggle="tab" href="#m_tabs_5_1">
                       Banner Bilgileri
                    </a>
                </li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active show" id="m_tabs_5_1" role="tabpanel">

                    <div id="app" class="m-portlet__body">
                        <div class="row">
                            <div class="col s12"> <image  class="col l3"  style="width: 100%" v-bind:src="resim"></image></div>

                            <input type="hidden" name="resim" v-bind:value="resim">
                            <div class="form-group m-form__group row">
                                <label class="col-form-label col-lg-3 col-sm-12">
                                  Banner Görseli
                                </label>
                                <div class="col-lg-7 col-md-9 col-sm-12">
                                    <input type="file" class="btn btn-outline-info btn-lg" onchange="readURL(this);" style="width: 100%" id="inputs" name="image" accept="image/*">


                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group m-form__group">
                                    <label>
                                        Sıra
                                    </label>
                                    <select name="sira" class="form-control m-input">
                                        @for($i=1;$i<30;$i++)
                                            <option value="{{$i}}">
                                                <b> {{ $i}}</b>
                                            </option>
                                        @endfor
                                    </select>
                                </div>
                                <div class="form-group m-form__group">
                                    <label for="exampleSelect1">
                                        Görüntüleneceği Yer
                                    </label>
                                    <select name="konum" class="form-control m-input" id="exampleSelect1">
                                        @foreach($konumlar as $konum)
                                            <option value="{{$konum->id}}">
                                                <b> {{$konum->ad}}</b> ({{$konum->aciklama}})
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
@if(false)
                                <div class="form-group m-form__group">
                                    <label for="typead1">
                                        Ad
                                    </label>

                                    <div class="m-typeahead " id="typead1">
                                        <input class="form-control m-input" name="ad" id="urunler" type="text"
                                               >
                                    </div>


                                </div>
                                <div class="form-group m-form__group">
                                    <label for="typead1">
                                     Açıklama
                                    </label>

                                    <div class="m-typeahead " id="typead1">
                                        <input class="form-control m-input" name="aciklama" id="urunler" type="text"
                                               >
                                    </div>


                                </div>
                                <div class="form-group m-form__group">
                                    <label for="typead1">
                                       Url
                                    </label>

                                    <div class="m-typeahead " id="typead1">
                                        <input class="form-control m-input" name="url" id="urunler" type="text"
                                        >
                                    </div>


                                </div>
    @endif




                            </div>

                        </div>
                    </div>
                    <div class="m-portlet__foot m-portlet__foot--fit">
                        <div class="m-form__actions m-form__actions">

                        </div>
                    </div>

                </div>

                <div class="row">
                    <div>
                        <button type="submit" class="btn btn-success">
                            Kaydet
                        </button>

                    </div>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>

@endsection
@section('js')
    <script src="/tema/assets/demo/default/custom/components/base/sweetalert2.js" type="text/javascript"></script>

    @if (session('status')=='1')
        <script> Swal('Başarılı', 'İşlem Başarılı', 'success');</script>
    @elseif(session('status')=='0')
        <script> Swal('Başarısız', 'İşlem Sırasında Bir Sorun Oluştu', 'error');</script>
    @elseif(session('status')=='2')
        <script> Swal('Başarısız', 'Lütfen Ürün Seçimi Yapınız', 'error');</script>
    @endif

    <script>

        var app = new Vue({
            el: '#app',
            data: {
              resim:'',
            },
            methods: {}
        });
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    app.resim=e.target.result;

                };

                reader.readAsDataURL(input.files[0]);
            }
        }

        function validateForm() {
            var sonuc = true;
            if (app.secilenurun.id == '') {
                Swal('Dikkat', 'Lütfen Ürün Seçimi Yapınız.', 'error');
                sonuc = false;
            }


            return sonuc;
        }
    </script>
@endsection