
    <header class="header">

        <nav class="navbar navbar-default navbar-fixed-top p-0">
            <div class="container bg-white bb">
                <div class="navbar-header">

                    <div class="top_bar_contact_item mr-3">
                        <img class="img-fluid mr-1" src="/images/icons/phone.png" alt="" width="20">
                        <a id="phone" href="tel:0312 278 38 60">(0312) 278 38 60</a>
                    </div>
                    <div class="top_bar_contact_item mr-3">
                        <img class="img-fluid mr-1" src="/images/icons/email.png" alt="" width="32">
                        <a href="mailto:info@mercedesparcadeposu.com">info@mercedesparcadeposu.com</a>
                    </div>
                    <div class="top_bar_contact_item ">
                        <img class="img-fluid" src="/images/icons/location-pin.png" alt="" width="20">
                        <a href="/iletisim">Bize Ulaşın</a>
                    </div>

                </div>
            </div>
        </nav>

        <div class="header_main">
            <div class="container pt-2 pb-2 bg-white bb">
                <div class="row">
                    <div class="col-lg-8 col-md-12 order-1">
                        <div class="logo_container">
                            <div class="logo">
                                <div class="row pr-3 pl-3">
                                    <div class="col-5 vm">
                                        <a href="/">
                                            <img class="img-fluid" src="/layout/images/logo.png" alt="">
                                        </a>
                                    </div>
                                    <div class="col-3 vm">
                                        <img class="img-fluid" src="/layout/images/logomercedes.png" alt="">
                                    </div>
                                    <div class="col-4 vm">
                                        <img class="img-fluid" src="/layout/images/logosmart.png" alt="">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="col-lg-4 col-12 order-lg-2 order-3 text-lg-left text-right ml-auto">
                        <div class="header_search">
                            <div class="header_search_content">
                                <div class="header_search_form_container">
                                    {!! Form::open(['route'=>'urunara','method'=>'POST','class'=>'header_search_form clearfix']) !!}

                                    <input type="search" required="required" class="header_search_input" name="ara" placeholder="Ürün ara...">
                                    <button type="submit" class="btn btn-warning header_search_button trans_300" value="Submit">
                                        <img src="https://res.cloudinary.com/dxfq3iotg/image/upload/v1560918770/search.png" alt=""></button>
                                    {!! Form::close() !!}
                                </div>
                            </div>
                        </div>
                    </div> 

                </div>
            </div>
        </div> 


        <nav class="main_nav m-0">
            <div class="container p-0">



                <nav class="navbar navbar-expand-lg navbar-light bg-light">
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>

                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav mr-auto">
                            <li class="nav-item">
                                <a class="nav-link {{ (request()->is('/')) ? 'active' : '' }}" href="/">Anasayfa <span class="sr-only">(current)</span></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link {{ (request()->segment(1) == 'urunler') ? 'active' : '' }}" href="/urunler">Ürünler <span class="sr-only">(current)</span></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link {{ (request()->segment(1) == 'markalar') ? 'active' : '' }}" href="/markalar">Markalar</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link {{ (request()->segment(1) == 'hizmetler') ? 'active' : '' }}" href="/hizmetler">Hizmetler</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link {{ (request()->segment(1) == 'musteri-hizmetleri') ? 'active' : '' }}" href="/musteri-hizmetleri">Müşteri Hizmetleri</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link {{ (request()->segment(1) == 'hakkimizda') ? 'active' : '' }}" href="/hakkimizda">Hakkımızda</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link {{ (request()->segment(1) == 'iletisim') ? 'active' : '' }}" href="/iletisim">İletişim</a>
                            </li>
                        </ul>

                    </div>
                </nav>


            </div>
        </nav> 
    </header>
