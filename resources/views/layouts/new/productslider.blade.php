<div class="container products">
    <div class="row">

        <div class="col-md-8">

            <h2>Öne Çıkan <b>Ürünler</b>
                <a class="carousel-control-prev" href="#productCarousel" data-slide="prev">
                    <i class="fas fa-chevron-circle-left"></i>
                </a>
                <a class="carousel-control-next" href="#productCarousel" data-slide="next">
                    <i class="fas fa-chevron-circle-right"></i>
                </a>
            </h2>

            <div id="productCarousel" class="carousel slide" data-ride="carousel" data-interval="4000">

                <div class="carousel-inner">

                    @foreach($anasayfaurun as $key => $urun)

                    @if ($key == 0 || $key % 9 == 0 && $key > 8)
                    @if ($key == 0 )
                    <div class="carousel-item active">
                        <div class="row m-0">
                            @else
                            <div class="carousel-item">
                                <div class="row m-0">
                                    @endif
                                    @endif

                                    <div class="col-sm-4 mb-2" style="padding: 2px">
                                        <div class="owl-item">
                                            <div class="single_product shadow-sm">
                                                <div class="product_name">
                                                    <p class="manufacture_product"><a href="/urun/{{$urun->id}}">{{$urun->ad}}</a></p>
                                                </div>
                                                <div class="product_thumb">
                                                    <a class="primary_img" href="/urun/{{$urun->id}}"><img src="{{$urun->resim}}" alt=""></a>
                                                </div>
                                                <div class="product_code">
                                                    <p class="mb-0">{{$urun->kod}}</p>
                                                    <p class="mb-0">{{$urun->marka->ad}}</p>
                                                    <p>{{$urun->araba_model}}</p>
                                                </div>

                                                <a class="btn-itl" href="/urun/{{$urun->id}}">
                                                    <span>İncele <i class="fas fa-arrow-circle-right text-warning fa-lg"></span></i>
                                                </a>
                                            </div>
                                        </div>
                                    </div>

                                    @if($key != 0 && $key % 9 == 8)
                                </div>

                            </div>
                            @endif

                            @endforeach

                        </div>
                    </div>
                </div>



                <div class="col-md-4">
                    <h2>Fırsat <b>Ürünleri</b></h2>

                    @foreach($firsaturun as $key => $urun)

                    <div class="single-wid-product mb-1 shadow-sm">
                        <a href="single-product.htm"> <img src="{{$urun->resim}}" alt="" class="product-thumb" pagespeed_url_hash="1733883786" onload="pagespeed.CriticalImages.checkImageForCriticality(this);"></a>
                        <p class="manufacture_product"><a href="/urun/{{$urun->id}}">{{$urun->ad}}</a></p>
                        <div class="product_code">
                            <p class="mb-0"><b>Parça No : </b>{{$urun->kod}}</p>
                            <p class="mb-0"><b>Ürün Markası : </b>{{$urun->marka->ad}}</p>
                            <p><b>Araç Modeli : </b>{{$urun->araba_model}}</p>
                        </div>
                    </div>
                    @endforeach

                </div>
            </div>
        </div>
    </div>
</div>