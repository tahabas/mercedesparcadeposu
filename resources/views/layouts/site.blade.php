

<!DOCTYPE html>
<html lang="tr">
    <head>
        <link rel="stylesheet" type="text/css" href="{{ asset('new/plugins/bootstrap/css/bootstrap.min.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ asset('new/css/style.min.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ asset('new/plugins/fontawesome/css/all.min.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ asset('new/plugins/OwlCarousel/dist/assets/owl.theme.default.min.css') }}">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        @yield('css')

    </head>
    <body>
        @include('layouts.new.header') 

        <div class="container pt-3 pb-3 bg-white">

            <img class="img-fluid m-auto" style="display: flow-root;"  src="/images/200.gif" alt="">

            <div class="row d-none">
                <div class="col-lg-3 col-md-12">
                    <div class="categories_menu">
                        <div class="categories_title">
                            <h2 class="categori_toggle">Kategoriler</h2>
                        </div>
                        <div class="categories_menu_toggle ">
                            <ul class="list-unstyled">
                                @foreach($sol_menuler as $sol_menu)
                                @if(count($sol_alt_menuler[$sol_menu->id])>0 )
                                <li class="menu_item_children categorie_list">
                                    <span>
                                        {{$sol_menu->ad}}
                                        <i class="fa fa-angle-right"></i>
                                        <span class="expand"></span>
                                    </span>

                                    <ul class="categories_mega_menu list-unstyled">

                                        @foreach($sol_alt_menuler[$sol_menu->id] as $sol_alt_menu)
                                        <li>
                                            <a style="font-size: 16px;" href="/urunler/{{$sol_alt_menu->id}}?sorgu=kategori">{{$sol_alt_menu->ad}}</a>
                                        </li>

                                        @endforeach

                                    </ul>
                                </li>
                                @else
                                @endif
                                @endforeach

                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-lg-9 col-md-12">

                    @yield('content')

                </div>
            </div>
        </div>

        @include('layouts.new.footer') 

    </body>
</html>

<script src="{{ asset('new/plugins/jquery.min.js')}}"></script>
<script src="{{ asset('js/aps.js')}}"></script>
<script src="{{ asset('new/plugins/bootstrap/js/bootstrap.min.js')}}"></script>
<script src="{{ asset('new/plugins/OwlCarousel/dist/owl.carousel.min.js')}}"></script>
@yield('js')