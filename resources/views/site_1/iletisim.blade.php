@extends('layouts.site')
@section('css')
@endsection
@section('slider')
    <div class="mail">
        <h3>İletişim Form</h3>
        <div class="agileinfo_mail_grids">
            <div class="col-md-6 agileinfo_mail_grid_left">
                <ul>
                    <li><i class="fa fa-home" aria-hidden="true"></i></li>
                    <li><span></span></li>
                </ul>
                <ul>
                    <li><i class="fa fa-envelope" aria-hidden="true"></i></li>
                    <li>E-Posta<span><a href="mailto:info@mercedesparcadeposu.com">info@mercedesparcadeposu.com</a></span></li>
                </ul>
                <ul>
                    <li><i class="fa fa-phone" aria-hidden="true"></i></li>
                    <li>Telefon<span>0312 278 64 40</span></li>
                </ul>
            </div>
            <div class="col-md-6 agileinfo_mail_grid_right">
                {!! Form::open(['route'=>'iletisim.mailgonder','method'=>'POST','class'=>'form-horizontal']) !!}
                    <div class="col-md-6 wthree_contact_left_grid">
                        <input type="text" name="ad" placeholder="Ad Soyad"  required>
                        <input type="email" name="mail" placeholder="E-Posta Adresi"  required>
                    </div>
                    <div class="col-md-6 wthree_contact_left_grid">
                        <input type="text" name="telefon" placeholder="Telefon Numarası" required>
                        <input type="text" name="konu" placeholder="Konu"  required="">
                    </div>
                    <div class="clearfix"> </div>
                    <textarea name="mesaj" placeholder="Mesajınız" required></textarea>
                    <input type="submit" value="Gönder">
                    <input type="reset" value="Temizle">
                {!! Form::close() !!}
            </div>


        </div>
        <div class="row">
            <div class="col-md-12">
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3059.3436510497327!2d32.70938721538132!3d39.93370217942424!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x14d348024de1dfd1%3A0xbf681654feb3d34a!2sBah%C3%A7ekap%C4%B1+Mahallesi%2C+2473.+Cadde+No%3A24%2C+06797+Etimesgut%2FAnkara!5e0!3m2!1str!2str!4v1535719214239" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>

            </div>
        </div>
        <div class="clearfix"> </div>
    </div>
@endsection
@section('icerik')

@endsection
@section('js')
@endsection
