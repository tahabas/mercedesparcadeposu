@extends('layouts.site')
@section('css')
@endsection
@section('slider')
    @if($urun->kategori->resim!='')
    <div class="w3l_banner_nav_right_banner3" style="background: url({{$urun->kategori->resim}}) no-repeat 0px 0px">
        <h3>{{$urun->kategori->ad}}</h3>
    </div>
    @endif
    <div class="agileinfo_single">
        <h5>{{$urun->ad}}</h5>
        <div class="col-md-4 agileinfo_single_left">
            <img id="example" src="{{$urun->resim}}" alt=" " class="img-responsive">
        </div>
        <div class="col-md-8 agileinfo_single_right">

            <div class="snipcart-item block">

                <div class="snipcart-thumb agileinfo_single_right_snipcart">
                    <table style="width:50%">
                        <tr>
                            <th></th>
                            <th></th>

                        </tr>
                        <tr>
                            <td><b>Kod</b></td>
                            <td><span>{{$urun->kod}}</span></td>
                        </tr>
                        <tr>
                            <td><b>Stok</b></td>
                            <td><span>{{$urun->stok}}</span></td>
                        </tr>
                        <tr>
                            <td><b>Araç Modeli</b></td>
                            <td><span>{{$urun->araba_model}}</span></td>
                        </tr>
                        <tr>
                            <td><b>Model Yılı</b></td>
                            <td><span>{{$urun->araba_yil}}</span></td>
                        </tr>
                        <tr>
                            <td><b>Kasa tipid</b></td>
                            <td><span>{{$urun->araba_kasa}}</span></td>
                        </tr>
                        <tr>
                            <td><b>Ürün Markası</b></td>
                            <td><span>{{$urun->marka->ad}}</span></td>
                        </tr>
                    </table>
                </div>

            </div>
            <div class="w3agile_description">
                @if($urun->aciklama!='')
                <h4>Açıklama :</h4>
               <div>{{$urun->aciklama}}</div>
                    @endif
            </div>

        </div>
        <div class="col-md-12">
            <!-- #######  YAY, I AM THE SOURCE EDITOR! #########-->
            <!-- #######  YAY, I AM THE SOURCE EDITOR! #########-->
            <div>
                <p align="center"><strong><span style="color: #333333;"><span style="font-family: Open Sans, sans-serif;"><span style="font-size: small;">PAR&Ccedil;A NO : {{$urun->kod}}</span></span></span></strong></p>
                <p align="center"><strong><span style="color: #333333;"><span style="font-family: Open Sans, sans-serif;"><span style="font-size: small;">DİGER PARCALAR CIKMA VE SIFIR OLARAK MEVCUTTUR...</span></span></span></strong></p>
                <p align="center"><strong><span style="color: #333333;"><span style="font-family: Open Sans, sans-serif;"><span style="font-size: small;">ANKARA İ&Ccedil;İ İ&Ccedil;İN AYNI G&Uuml;N ADRES TESLİMİ YAPILIR...</span></span></span></strong></p>
                <p align="center"><span style="color: #333333;"> <strong><span style="font-family: Open Sans, sans-serif;"><span style="font-size: small;">DİĞER İLLER İ&Ccedil;İN AYNI G&Uuml;N U&Ccedil;AK KARGO  VE</span></span></strong></span></p>
                <p align="center"><strong><span style="color: #333333;"><span style="font-family: Open Sans, sans-serif;"><span style="font-size: small;">ANLASMALI KARGOLARIMIZLA PAR&Ccedil;A G&Ouml;NDERİLMEKTEDİR...</span></span></span></strong></p>
                <p align="center"><strong><span style="color: #333333;"><span style="font-family: Open Sans, sans-serif;"><span style="font-size: small;">KAPIDA &Ouml;DEME SİSTEMİMİZ VARDIR.</span></span></span></strong></p>

                <p align="center"><strong><span style="color: #333333;"><span style="font-family: Open Sans, sans-serif;"><span style="font-size: small;">www.meranmercedes.com</span></span></span></strong></p> <p style="text-align: center;"><span style="font-size: x-large;"><strong>0312 278 64 40</strong></span></p>
            </div>
            <img src="/images/kargolar.png">
        </div>
        <div class="clearfix"> </div>
    </div>

@endsection
@section('icerik')

@endsection
@section('js')
@endsection
