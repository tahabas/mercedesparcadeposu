@extends('layouts.site')
@section('css')
@endsection
@section('slider')
    <div class="container">
        @if(count($urunler)>0)
            <h3>"{{$cumle}}" Arama sonuçları</h3>
        @endif
        @if(count($urunler)<=0)
            <h3>"{{$cumle}}"Aramanız ile eşleşen ürün bulunamadı.</h3>
        @endif
        <div class="agile_top_brands_grids">

            @foreach($urunler as $urun)
                <div class="col-md-3 top_brand_left">
                    <div class="hover14 column">
                        <div class="agile_top_brand_left_grid">
                            <div class="agile_top_brand_left_grid1">
                                <figure>
                                    <div class="snipcart-item block">
                                        <div class="snipcart-thumb">
                                            <a href="/urun/{{$urun->id}}"><img title=" " alt=" " height="140px"
                                                                               src="{{$urun->resim}}"/></a>
                                            <p>{{$urun->ad}}</p>
                                            <h4>Kod: {{$urun->kod}}</h4>
                                        </div>
                                        <div class="snipcart-details top_brand_home_details">
                                            <a href="/urun/{{$urun->id}}">
                                                <input type="button" name="submit" value="Detay" class="button"/>
                                            </a>
                                        </div>
                                    </div>
                                </figure>
                            </div>
                        </div>
                    </div>
                </div>

            @endforeach


            <div class="clearfix"></div>
        </div>
    </div>
@endsection

@section('js')
@endsection