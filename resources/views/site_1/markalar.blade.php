@extends('layouts.site')
@section('css')
@endsection
@section('slideralt')
    <div class="container">
        <div class="w3ls_w3l_banner_nav_right_grid w3ls_w3l_banner_nav_right_grid_sub">
            <h3 class="w3l_fruit">Markalar</h3>
            <div class="w3ls_w3l_banner_nav_right_grid1 w3ls_w3l_banner_nav_right_grid1_veg">
                @foreach($markalar as $marka)
                    <div class="col-md-2 w3ls_w3l_banner_left w3ls_w3l_banner_left_asdfdfd">
                        <div class="hover14 column">
                            <div class="agile_top_brand_left_grid w3l_agile_top_brand_left_grid">
                                <div class="agile_top_brand_left_grid_pos">

                                </div>
                                <div class="agile_top_brand_left_grid1">
                                    <figure>
                                        <div class="snipcart-item block">
                                            <div class="snipcart-thumb">
                                                <a href="urunler/{{$marka->id}}?sorgu=marka"><img src="{{$marka->resim}}" alt=" " class="img-responsive" /></a>
                                                <a href="urunler/{{$marka->id}}?sorgu=marka"> <p>{{$marka->ad}}({{$marka->urun->count()}})</p></a>

                                            </div>

                                        </div>
                                    </figure>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
                <div class="clearfix"> </div>
            </div>


        </div>
    </div>
    @endsection
@section('icerik')

@endsection
@section('js')
@endsection