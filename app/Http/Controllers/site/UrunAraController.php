<?php

namespace App\Http\Controllers\site;

use App\models\urun;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UrunAraController extends Controller
{
    public  function  UrunAra(Request $request)
    {
        $cumle=request('ara');
        $urunler=urun::where('aktif',1)->where('ad','like','%'.$cumle.'%')->orWhere('kod','like','%'.$cumle.'%')->orderBy('ad','desc')->get();
        return view('site.urun.aramasonuc',compact('urunler','cumle'));
    }
}
