<?php

namespace App\Http\Controllers\site;

use App\kategori;
use App\models\marka;
use App\models\urun;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UrunController extends Controller
{
    public  function  listele($kategori)
    {
        $ltur=request('sorgu');
        $urunler=urun::where('aktif',1);
        $urunkategori='';
        if ($ltur=='marka')
        {
            $urunler=$urunler->where('marka_id',$kategori)->get();
            $urunkategori=marka::where('aktif',1)->where('id',$kategori)->first();
        }
        else
        {
            $urunler=$urunler->where('kategori_id',$kategori)->get();
            $urunkategori=kategori::where('aktif',1)->where('id',$kategori)->first();

        }


        return view('site.urun.listele',compact('urunler','urunkategori'));
    }
    public  function  detay($id)
    {
        $urun=urun::where('aktif',1)->where('id',$id)->first();


        return view('site.urun.detay',compact('urun'));
    }
    
    
     public  function  urunler()
    {
        $urunler=urun::where('aktif',1)->paginate(12);

        return view('site.urun.urunler',['urunler' => $urunler]);
    }
    
    
}
