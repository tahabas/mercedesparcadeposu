<?php

namespace App\Http\Controllers\site;

use App\models\marka;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MarkaController extends Controller
{
    public  function  index()
    {
        $markalar=marka::where('aktif',1)->get();
        return view('site.markalar',compact('markalar'));
    }
}
