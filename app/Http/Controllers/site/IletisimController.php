<?php

namespace App\Http\Controllers\site;

use App\models\mail_list;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;

class IletisimController extends Controller
{
    public  function  MailGonder(Request $request)
    {
        $ad=request('ad');
        $telefon=request('telefon');
        $mail=request('mail');
        $konu=request('konu');
        $mesaj=request('mesaj');

        $data = array
        (
            'name'=>$ad,
            'email'=>$mail,
            'phone'=>$telefon,
            'subject'=>$konu,
            'messagetext'=>$mesaj
        );
        Mail::send('site.mail.iletisim', ['title' => 'İletişim formu: '.$ad.' '.$konu, 'content' => $data], function ($message) use ($data) {

            $message->from('info@mercedesparcadeposu.com');

            $message->subject($data['subject']);

            $message->to($data['email']);


        });
        return back()->with('status',1);
    }
    public  function  MailEkle(Request $request)
    {
        $mail=request('mail');
        $varmi=mail_list::where('mail',$mail)->first();
        if (!$varmi)
        {
            $yeni=new mail_list();
            $yeni->mail=$mail;
            $yeni->save();
        }
        return back()->with('status',1);

    }

}
