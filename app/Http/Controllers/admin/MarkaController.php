<?php

namespace App\Http\Controllers\admin;

use App\models\marka;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Intervention\Image\ImageManager;

class MarkaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $markalar = marka::where('aktif', 1);
        $ara = $request->query('ara');
        if (isset($ara)) {

            $markalar= $markalar->where('ad', 'like', '%' . $ara . '%');
        }
        $markalar = $markalar->paginate(200);
        $markalar->appends($_GET);
        return view('admin.marka.listele', compact( 'markalar'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.marka.ekle');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        try{
            $marka = new marka();


            $marka-> ad = $request -> input('txtMarkaAdi');
            try
            {
                try
                {
                    \Illuminate\Support\Facades\File::makeDirectory(public_path() . '/images/markagorsel');
                }
                catch (\Exception $e)
                {

                }
                $file = str_replace('data:image/png;base64,', '', request('resim'));
                $file = str_replace('data:image/jpeg;base64,', '', $file);
                $data = base64_decode($file);
                $dosyaadi = \guid();
                $manager = new ImageManager();
                $manager->make($data)->resize(640, 640)->save(public_path() . '/images/markagorsel/'. $dosyaadi . '.jpg');
                $marka->resim='/images/markagorsel/' . $dosyaadi . '.jpg';
                $marka->save();
            }
            catch (\Exception $e)
            {
                error_log($e);
            }
            $marka->save();
            return back()->with('status', '1');
        }catch (\Exception $e){
            return back()->with('status', '0');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $marka = marka::where('aktif', 1)->find($id);
        return view('admin.marka.duzenle', compact( 'marka'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        try {
            $marka=marka::find($id);
            $marka->ad=request('txtMarkaAdi');
            if($request->has('chbInput'))
            {
                $marka->aktif_1=1;
            }
            try
            {
                try
                {
                    \Illuminate\Support\Facades\File::makeDirectory(public_path() . '/images/markagorsel');
                }
                catch (\Exception $e)
                {

                }




                $file = str_replace('data:image/png;base64,', '', request('resim'));
                $file = str_replace('data:image/jpeg;base64,', '',$file);
                $data = base64_decode($file);

                $dosyaadi = \guid();
                $manager = new ImageManager();
                $manager->make($data)->resize(1280, 720)->save(public_path() . '/images/markagorsel/'. $dosyaadi . '.jpg');
                $marka->resim='/images/markagorsel/' . $dosyaadi . '.jpg';
                $marka->save();
            }
            catch (\Exception $e)
            {
                error_log($e);
            }
            $marka->save();
            return back()->with('status', '1');
        }catch (\Exception $e){
            return back()->with('status', '0');
        }


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            $marka = marka::find($id);
            $marka -> aktif = 0;
            $marka->save();
            return back()->with('status', '1');
        }catch (\Exception $e){
            return back()->with('status', '0');
        }



    }
}
