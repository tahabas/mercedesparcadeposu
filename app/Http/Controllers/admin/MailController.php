<?php

namespace App\Http\Controllers\admin;

use App\models\mail_list;
use foo\bar;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;

class MailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $maillist=mail_list::where('aktif',1)->get();
        return view('admin.mail.mailgonder',compact('maillist'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $konu=request('konu');
        $mesaj=request('mesaj');

        $mailler = json_decode(request('mail'));
        foreach ($mailler as $item) {
            try{
                $data = array
                (
                    'mesaj'=>$mesaj,
                    'email'=>$konu,
                    'konu'=>$konu

                );
               Mail::send('admin.mail.mailsablon', ['title' => $konu, 'content' => $data], function ($message) use ($data) {

                    $message->from('info@mercedesparcadeposu.com');

                    $message->subject($data['konu']);

                    $message->to($data['email']);


                });
            }catch (\Exception $e)
            {

            }
        }
        return back()->with('status',1);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
