<?php

namespace App\Http\Controllers\admin;

use App\models\ziyaret_log;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RaporController extends Controller
{
    public function ZiyaretciRaporu(Request $request)
    {
        $postvar=request('tarih1');

        $tarih1 = request('tarih1');
        $tarih2 = request('tarih2');

        if (!isset($tarih1)) {
            $tarih1 = date('Y-m-d 00:00:00');
        }
        else
        {
           // $tarih1=date_parse($tarih1);
            $tarih1=$tarih1.' 00:00:00';
        }
        if (!isset($tarih2)) {
            $tarih2 = date('Y-m-d 23:59:59');
        }
        else{
           // $tarih2=date_parse($tarih2);
           $tarih2=$tarih2.' 23:59:59';
        }

        $data = ziyaret_log::where('aktif', 1)->where('created_at', '<=', $tarih2)->where('created_at', '>=', $tarih1)->orderBy('created_at', 'desc')->get();
        $bolgeler = [];
        $ipler = [];
        foreach ($data as $item) {
            $ipvar = false;
            $bolgevar = false;
            $secilibolge = null;
            $seciliip = null;
            foreach ($bolgeler as $key => $value) {
                if ($item->ulke == $key) {
                    $bolgevar = true;
                    $secilibolge = $key;
                }

            }

            if ($bolgevar == false) {
                //  array_push($bolgeler, ['ulke' => $item->ulke, 'adet' => 1]);
                $bolgeler[$item->ulke] = 1;

            } else {

                $bolgeler[$item->ulke] += 1;
            }
            foreach ($ipler as $key => $value) {
                if ($item->ip == $key) {
                    $ipvar = true;
                    $seciliip = $key;
                }

            }

            if ($ipvar == false) {
                //  array_push($bolgeler, ['ulke' => $item->ulke, 'adet' => 1]);
                $ipler[$item->ip] = 1;

            } else {

                $ipler[$item->ip] += 1;
            }
        }



        $tariharaligi= str_replace('00:00:00','',$tarih1).' - '.str_replace('23:59:59','',$tarih2);


        return view('admin.rapor.ziyaretci',compact('bolgeler','ipler','data','tariharaligi'));
    }
}
