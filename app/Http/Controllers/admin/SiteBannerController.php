<?php

namespace App\Http\Controllers\admin;

use App\models\site_banner;
use App\models\site_banner_konum;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Intervention\Image\ImageManager;

class SiteBannerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $konum=$request->query('konum');
        $bannerlist = site_banner::where('aktif', 1);
        $sonkonum=8;
        if (isset($konum))
        {
            $bannerlist->where('konum',$konum);
            $sonkonum=$konum;
        }else{
            $bannerlist->where('konum',$sonkonum);
            $sonkonum=$konum;
        }
        $bannerlist=$bannerlist ->paginate(30);
        $konumlar=site_banner_konum::where('aktif',1)->get();
        return view('admin.banner.listele', compact('bannerlist','konumlar','sonkonum'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $konumlar = site_banner_konum::where('aktif', 1)->get();
        return view('admin.banner.ekle', compact('konumlar'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try
        {

          //  $ad=request('ad');
            $konum=request('konum');
          //  $aciklama=request('aciklama');
         //   $url=request('url');
            // $resim=request('resim');
            $banner=new site_banner();
          //  $banner->ad=$ad;
          //  $banner->aciklama=$aciklama;
          //  $banner->url=$url;
            $banner->konum=$konum;
            try
            {
                try
                {
                    // \Illuminate\Support\Facades\File::makeDirectory(public_path() . '/images/kategorigorsel');
                }
                catch (\Exception $e)
                {

                }
                $file = str_replace('data:image/png;base64,', '', request('resim'));
                $file = str_replace('data:image/jpeg;base64,', '', $file);
                $data = base64_decode($file);
                $dosyaadi = \guid();
                $manager = new ImageManager();
                $manager->make($data)->save(public_path() . '/images/banner/'. $dosyaadi . '.jpg');
                $banner->resim='/images/banner/' . $dosyaadi . '.jpg';

            }
            catch (\Exception $e)
            {
                error_log($e);
            }
            $banner->save();
            return back()->with('status', '1');
        } catch (\Exception $e) {
            error_log($e);
            return back()->with('status', '0');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $banner=site_banner::find($id);
        $konumlar = site_banner_konum::where('aktif', 1)->get();
        return view('admin.banner.duzenle',compact('banner','konumlar'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try
        {

          //  $ad=request('ad');
            $konum=request('konum');
         //   $aciklama=request('aciklama');
         //   $url=request('url');
            // $resim=request('resim');
            $banner= site_banner::find($id);
        //    $banner->ad=$ad;
         //   $banner->aciklama=$aciklama;
         //   $banner->url=$url;
            $banner->konum=$konum;
            try
            {
                try
                {
                    // \Illuminate\Support\Facades\File::makeDirectory(public_path() . '/images/kategorigorsel');
                }
                catch (\Exception $e)
                {

                }
                $file = str_replace('data:image/png;base64,', '', request('resim'));
                $file = str_replace('data:image/jpeg;base64,', '', $file);
                $data = base64_decode($file);
                $dosyaadi = \guid();
                $manager = new ImageManager();
                $manager->make($data)->save(public_path() . '/images/banner/'. $dosyaadi . '.jpg');
                $banner->resim='/images/banner/' . $dosyaadi . '.jpg';

            }
            catch (\Exception $e)
            {
                error_log($e);
            }
            $banner->save();
            return back()->with('status', '1');
        } catch (\Exception $e) {
            error_log($e);
            return back()->with('status', '0');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $banner=site_banner::find($id);
        $banner->aktif=0;
        $banner->save();
        return back();
    }
}
