<?php

namespace App\Http\Controllers\admin;

use App\kategori;
use App\models\marka;
use App\models\urun;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Intervention\Image\ImageManager;

class UrunController extends Controller
{
    protected function AltGetir($id, $kategori)
    {
        $altkategori = kategori::where('ust_id', $id)->where('aktif','1')->get();
        $altkategoriler = array();
        foreach ($altkategori as $key) {
            $this->AltGetir($key->id, $key);
            array_push($altkategoriler, $key);
        }
        $kategori->altkategoriler = $altkategoriler;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $urunler = urun::where('aktif', 1);
        $ara = $request->query('ara');
        if (isset($ara)) {

            $urunler= $urunler->where('ad', 'like', '%' . $ara . '%');
        }
        $urunler= $urunler->paginate(200);
        $urunler->appends($_GET);
        return view('admin.urun.listele', compact( 'urunler'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $markalar=marka::where('aktif',1)->get();
        $kategoriler=kategori::where('aktif',1)->get();
        foreach ($kategoriler as $kategori) {
            $this->AltGetir($kategori->id,$kategori);
        }
       return view('admin.urun.ekle',compact('markalar','kategoriler'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            $urun = new urun();


            $urun-> ad = $request -> input('ad');
            $urun-> kod = $request -> input('kod');
            $urun-> stok = $request -> input('stok');
            $urun-> araba_model = $request -> input('araba_model');
            $urun-> araba_yil = $request -> input('araba_yil');
            $urun-> araba_kasa = $request -> input('araba_kasa');
            $urun-> marka_id = $request -> input('marka_id');
            $urun-> kategori_id = $request -> input('kategori_id');
            $urun-> aciklama = $request -> input('aciklama');
            try
            {
                try
                {
                    \Illuminate\Support\Facades\File::makeDirectory(public_path() . '/images/urungorsel');
                }
                catch (\Exception $e)
                {

                }
                $file = str_replace('data:image/png;base64,', '', request('resim'));
                $file = str_replace('data:image/jpeg;base64,', '', $file);
                $data = base64_decode($file);
                $dosyaadi = \guid();
                $manager = new ImageManager();
                $manager->make($data)->resize(1280, 720)->save(public_path() . '/images/urungorsel/'. $dosyaadi . '.jpg');
                $urun->resim='/images/urungorsel/' . $dosyaadi . '.jpg';
                $urun->save();
            }
            catch (\Exception $e)
            {
                error_log($e);
            }
            $urun->save();
            error_log('kaydet');
            return back()->with('status', '1');
        }catch (\Exception $e){
            return back()->with('status', '0');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $markalar=marka::where('aktif',1)->get();
        $kategoriler=kategori::where('aktif',1)->get();
        $urun=urun::find($id);
        foreach ($kategoriler as $kategori) {
            $this->AltGetir($kategori->id,$kategori);
        }
        return view('admin.urun.duzenle',compact('markalar','kategoriler','urun'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try{
            $urun =urun::find($id);


            $urun-> ad = $request -> input('ad');
            $urun-> kod = $request -> input('kod');
            $urun-> stok = $request -> input('stok');
            $urun-> araba_model = $request -> input('araba_model');
            $urun-> araba_yil = $request -> input('araba_yil');
            $urun-> araba_kasa = $request -> input('araba_kasa');
            $urun-> marka_id = $request -> input('marka_id');
            $urun-> kategori_id = $request -> input('kategori_id');
            $urun-> aciklama = $request -> input('aciklama');
            try
            {
                try
                {
                    \Illuminate\Support\Facades\File::makeDirectory(public_path() . '/images/urungorsel');
                }
                catch (\Exception $e)
                {

                }
                $file = str_replace('data:image/png;base64,', '', request('resim'));
                $file = str_replace('data:image/jpeg;base64,', '', $file);
                $data = base64_decode($file);
                $dosyaadi = \guid();
                $manager = new ImageManager();
                $manager->make($data)->resize(1280, 720)->save(public_path() . '/images/urungorsel/'. $dosyaadi . '.jpg');
                $urun->resim='/images/urungorsel/' . $dosyaadi . '.jpg';
                $urun->save();
            }
            catch (\Exception $e)
            {
                error_log($e);
            }
            $urun->save();
            return back()->with('status', '1');
        }catch (\Exception $e){
            return back()->with('status', '0');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            $marka = urun::find($id);
            $marka -> aktif = 0;
            $marka->save();
            return back()->with('status', '1');
        }catch (\Exception $e){
            return back()->with('status', '0');
        }
    }
}
