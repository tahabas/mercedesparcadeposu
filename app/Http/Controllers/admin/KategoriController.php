<?php

namespace App\Http\Controllers\admin;

use App\kategori;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Intervention\Image\ImageManager;

class KategoriController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $kategoriler = kategori::where('ust_id',0)->where('aktif',1)->orderBy('sira','asc')->get();
        foreach ($kategoriler as $kategori1) {
            $this->AltGetir($kategori1->id, $kategori1);
        }

        return view('admin.kategori.listele', compact('kategoriler'));
    }

    protected function AltGetir($id, $kategori)
    {
        $altkategori = kategori::where('ust_id', $id)->where('aktif','1')->orderBy('sira','asc')->get();
        $altkategoriler = array();
        foreach ($altkategori as $key) {
            $this->AltGetir($key->id, $key);
            array_push($altkategoriler, $key);
        }
        $kategori->altkategoriler = $altkategoriler;
    }
    public function create()
    {
        $kategoriler = kategori::all();
        return view('admin.kategori.ekle', compact('kategoriler'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {

            $kategori = new kategori();

            $kategori->ust_id = request('txtUstKategori');
            $kategori->ad = request('txtKategoriAdi');
            $ustvar=request('txtUstKategori');
            $sira=request('sira');
            $kategori->sira=$sira;
            if (!isset($ustvar))
            {
                $kategori->ust_id=0;
            }

            $kategori->save();

            try
            {
                try
                {
                    \Illuminate\Support\Facades\File::makeDirectory(public_path() . '/images/kategorigorsel');
                }
                catch (\Exception $e)
                {

                }
                $file = str_replace('data:image/png;base64,', '', request('kategoriresmi'));
                $file = str_replace('data:image/jpeg;base64,', '', $file);
                $data = base64_decode($file);
                $dosyaadi = \guid();
                $manager = new ImageManager();
                $manager->make($data)->resize(1280, 300)->save(public_path() . '/images/kategorigorsel/'. $dosyaadi . '.jpg');
                $kategori->resim='/images/kategorigorsel/' . $dosyaadi . '.jpg';
                $kategori->save();
            }
            catch (\Exception $e)
            {
error_log($e);
            }
            return back()->with('status', '1');
        } catch (\Exception $e) {
            error_log($e);
            return back()->with('status', '0');

        }


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        try {
            $tumkategoriler = kategori::all();
            $kategori = kategori::find($id);
            return view('admin.kategori.duzenle', compact('kategori', 'tumkategoriler'));
        } catch (\Exception $e) {

        }

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $kategori = kategori::find($id);


            $kategori->ust_id = $request->input('chbUstKategori');
            $ustvar=request('chbUstKategori');
            $sira=request('sira');
            $kategori->sira=$sira;
            if (!isset($ustvar))
            {
                $kategori->ust_id=0;
            }
            $kategori->ad = $request->input('txtKategoriAdi');

            $kategori->save();

            try
            {
                try
                {
                    \Illuminate\Support\Facades\File::makeDirectory(public_path() . '/images/kategorigorsel');
                }
                catch (\Exception $e)
                {

                }




                $file = str_replace('data:image/png;base64,', '', request('kategoriresmi'));
                $file = str_replace('data:image/jpeg;base64,', '',$file);
                $data = base64_decode($file);

                $dosyaadi = \guid();
                $manager = new ImageManager();
                $manager->make($data)->resize(1280, 300)->save(public_path() . '/images/kategorigorsel/'. $dosyaadi . '.jpg');
                $kategori->resim='/images/kategorigorsel/' . $dosyaadi . '.jpg';
                $kategori->save();
            }
            catch (\Exception $e)
            {
error_log($e);
            }
            return back()->with('status', '1');
        } catch (\Exception $e) {
            error_log($e);
            return back()->with('status', '0');

        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $kategori = kategori::find($id);
            $kategori->aktif = 0;
            $kategori->save();

            return back()->with('status', 'İşlem Başarılı');
        } catch (\Exception $e) {
            return back()->with('status', 'İşlem Başarısız');
        }
    }

}
