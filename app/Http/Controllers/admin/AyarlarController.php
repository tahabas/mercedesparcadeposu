<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class AyarlarController extends Controller
{
    public  function  SifreDegistirGet()
    {
        return view('admin.ayarlar.sifre');
    }
    public function  SifreDegistir(Request $request)
    {
        $sifre=request('sifre');
        $sifre2=request('sifre2');
        if ($sifre!=$sifre2)
        {

            return back()->with('status',2);
        }
        $user=Auth::user();
        $user->password=bcrypt($sifre);
        $user->save();
        return back()->with('status',1);
    }
}
