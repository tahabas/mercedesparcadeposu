<?php

namespace App\Http\ViewComposers;

use App\models\admin_menu;
use App\models\urun;
use App\models\ziyaret_log;
use Illuminate\Contracts\View\View;
use App\kategori;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Request;


class GlobalComposer
{
    protected function sol_menu()
    {
        $this->menuler = kategori::where('aktif', 1)->where('ust_id', 0)->orderBy('ad','asc')->orderBy('sira','asc')->get();
		foreach($this->menuler as $menu){
			$this->alt_menuler[$menu->id] = kategori::where('aktif', 1)->where('ust_id', $menu->id)->orderBy('sira','asc')->get();
		}
    }
    protected function AdminMenuAltGetir($id, $menu)
    {
        $altmenu = admin_menu::where('ust_id', $id)->where('aktif', 1)->orderBy('sira','asc')->get();
        $altmenuler = array();
        foreach ($altmenu as $key) {
            $this->AdminMenuAltGetir($key->id, $key);
            array_push($altmenuler, $key);
        }
        $menu->altmenuler = $altmenuler;
    }
    public function compose(View $view)
    {
        app('debugbar')->disable();
		if (Auth::check())
        {
            $menuler = admin_menu::where('aktif', 1)->where('ust_id', 0)->orderBy('sira','asc')->get();

            foreach ($menuler as $item) {
                $altmenuler = [];
                $item->alt_menuler = [];


                $item->alt_menuler = $altmenuler;


            }
            $sonmenuler = [];
            foreach ($menuler as $item) {

                    array_push($sonmenuler, $item);

            }

            $url = \Illuminate\Support\Facades\Route::current()->uri;


            \view()->share(['amenuler' => $sonmenuler, 'url' => $url]);
        }


        if ($view->name() == 'layouts.site' ) {
            try
            {

                $clientip=Request::ip();
               // $clientip='176.234.4.1';
                // dd(unserialize(file_get_contents("http://ip-api.com/php/".$clientip)));
                $data=unserialize(file_get_contents("http://ip-api.com/php/".$clientip));
                if ($data && $data['status']=='success')
                    $yeni=new ziyaret_log();
                $yeni->ip=$clientip;
                $yeni->ulke=$data['country'];
                $yeni->ulke_kod=$data['countryCode'];
                $yeni->sehir=$data['city'];
                $yeni->posta_kod=$data['zip'];
                $yeni->plaka=$data['region'];
                $yeni->save();
            }
            catch (\Exception $e)
            {
                //error_log($e);
            }

            $this->sol_menu();
			\view()->share(['sol_menuler' => $this->menuler,'sol_alt_menuler' => $this->alt_menuler]);
        }
    }

}