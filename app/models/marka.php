<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class marka extends Model
{
    protected $table = 'marka';
    public function urun()
    {
        return $this->hasMany('App\models\urun', 'marka_id')->where('aktif', 1);
    }
}
