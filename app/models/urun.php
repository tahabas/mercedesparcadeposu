<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class urun extends Model
{
    protected $table = 'urun';
    public function marka()
    {
        return $this->belongsTo('App\models\marka', 'marka_id');
    }
    public function kategori()
    {
        return $this->belongsTo('App\kategori', 'kategori_id');
    }
}
