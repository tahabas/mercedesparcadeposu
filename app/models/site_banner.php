<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class site_banner extends Model
{
    protected $table = 'site_banner';
    public function konum()
    {
        return $this->belongsTo('App\site_banner_konum', 'konum');
    }
}
