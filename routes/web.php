<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {
    $firsaturun=\App\models\urun::where('aktif',1)->orderByRaw("RAND()")->take(6)->get();
    $anasayfaurun=\App\models\urun::where('aktif',1)->orderByRaw("RAND()")->take(18)->get();
    $slider=\App\models\site_banner::where('aktif',1)->get();
    return view('site.anasayfa',compact('firsaturun','anasayfaurun','slider'));
});
Route::get('/hakkimizda', function () {
    return view('site.hakkimizda');
});
Route::get('/iletisim', function () {
    return view('site.iletisim');
});
Route::get('urunler', 'site\UrunController@urunler')->name('urunler');
Route::get('/musteri-hizmetleri', function () {
    return view('site.musteri_hizmetleri');
});
Route::get('/hizmetler', function () {
    return view('site.hizmetler');
});
Auth::routes();
Route::resource('admin/banner', 'admin\SiteBannerController')->middleware(['auth']);
Route::post('/iletisim/mailgonder', 'site\IletisimController@MailGonder')->name('iletisim.mailgonder');
Route::post('/iletisim', 'site\IletisimController@MailEkle')->name('iletisim.mailekle');
Route::post('/urunara', 'site\UrunAraController@UrunAra')->name('urunara');
Route::get('/home', 'admin\DashboardController@index')->name('home')->middleware(['auth']);
Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');
Route::resource('admin/mail','admin\MailController')->middleware(['auth']);
Route::resource('admin/kategori','admin\KategoriController')->middleware(['auth']);
Route::resource('admin/marka','admin\MarkaController')->middleware(['auth']);
Route::resource('admin/urun','admin\UrunController')->middleware(['auth']);
Route::get('/admin', 'admin\DashboardController@index')->name('home')->middleware(['auth']);
Route::get('/dashboard', 'admin\DashboardController@index')->name('home')->middleware(['auth']);
Route::get('/markalar', 'site\MarkaController@index');
Route::get('/urunler/{id}', 'site\UrunController@listele');
Route::get('/urun/{id}', 'site\UrunController@detay');
Route::post('/admin/ayarlar/', 'admin\AyarlarController@SifreDegistir')->name('ayarlar.sifredegistir')->middleware(['auth']);
Route::get('/admin/ayarlar/sifredegistir', 'admin\AyarlarController@SifreDegistirGet')->middleware(['auth']);
Route::get('/admin/rapor/ziyaretci', 'admin\RaporController@ZiyaretciRaporu')->middleware(['auth']);
Route::post('/admin/rapor/ziyaretci', 'admin\RaporController@ZiyaretciRaporu')->name('admin.rapor.ziyaretci')->middleware(['auth']);